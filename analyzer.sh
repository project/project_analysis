#!/bin/bash
set -ux

# Arguments are
# 1: project name, 2: composer name, 3: release version,
# 4: type of extension, 5: number of workspace
MACHINE_NAME=${1}
COMPOSER_NAME=${2}
VERSION=${3}
EXTENTION_TYPE=${4}
WORKSPACE_NUMBER=${5}
COMPOSER_VERSION=${6}

VERSION=${COMPOSER_VERSION}

>&2 echo "\e[35mAnalyze ${MACHINE_NAME} ${VERSION} (drupal/${COMPOSER_NAME}:${COMPOSER_VERSION})\e[0m"

function gitCommit() {
  cd ${1} || exit 1
  git init
  git add .;git commit -q -m "git project before rector"
  cd - || exit 1
}

>&2 echo -e "\e[35mAnalyze ${1}\e[0m"

create_patch=0
export WORKSPACE_DIR="${WORKSPACE_DIR:-/build/drupalci/workspace}"
export CHECKOUT_BASE_DIR="${WORKSPACE_DIR}/drupal-checkouts"
CHECKOUT_DIR="${CHECKOUT_BASE_DIR}/drupal${WORKSPACE_NUMBER}"

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Extra logging for debugging
DF_OUT=$(df -hi; df -h; free --mega)

# Add the requested project to this composer environment (${WORKSPACE_NUMBER} is the number of this workspace, ${MACHINE_NAME} is project name, ${COMPOSER_NAME} is the composer name and ${VERSION} is the version).
cd $CHECKOUT_DIR || exit 1
export COMPOSER_HOME=${CHECKOUT_BASE_DIR}/composer-home${WORKSPACE_NUMBER}
COMPOSER_MEMORY_LIMIT=-1 composer --no-interaction --no-progress require drupal/${COMPOSER_NAME} ${COMPOSER_VERSION} --prefer-source --no-cache 2>> ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.phpstan_stderr

# Composer sometimes fails, lets try and make sure it tries again when it does.
if grep -q "Could not scan for classes" ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.phpstan_stderr; then
  >&2 echo -e "\e[35mComposer failed to install properly, reinstalling ${1}\e[0m"
  rm -rf vendor
  COMPOSER_MEMORY_LIMIT=-1 composer --no-interaction --no-progress install --prefer-install=dist
  COMPOSER_MEMORY_LIMIT=-1 composer --no-interaction --no-progress require drupal/${COMPOSER_NAME} ${COMPOSER_VERSION} --prefer-source --no-cache 2>> ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.phpstan_stderr
fi


# Ensure the directory was created where we thought it should be based on the project machine name.
if [[ -d "$CHECKOUT_DIR/${EXTENTION_TYPE#project_}s/contrib/${MACHINE_NAME}" ]]; then

  # We prefer source to work around the packaging issues of Drupal.org, but we dont want no .git directories.
  rm -rf "$CHECKOUT_DIR/${EXTENTION_TYPE#project_}s/contrib/${MACHINE_NAME}/.git"

  # Set file paths relative to our workspace, so they don't mix up between workspaces.
  php -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/drush --root=$CHECKOUT_DIR config-set system.file path.temporary $CHECKOUT_DIR/sites/default/files/temp

  # Enable the module based on the machine name.
  php -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/drush --root=$CHECKOUT_DIR en ${MACHINE_NAME} -y || true

  # Run Upgrade Status to get a baseline of results of current status.
  php -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/drush --root=$CHECKOUT_DIR upgrade_status:analyze --format=checkstyle ${MACHINE_NAME} > ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.upgrade_status.pre_rector.xml 2>> ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.upgrade_status_stderr1

  # Check if we need to run rector based on results from Upgrade Status.
  php -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/rector_needed ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.upgrade_status.pre_rector.xml
  rector_needed_result=$?
  info_updatable_result=1
  composer_json_updatable_result=1

  # set this back to 0 when we figure what we need to do with rector
  if [ $rector_needed_result -eq 0 ]; then
    # Create a git commit for the current state of the project, so we can diff clearly later.
    gitCommit ${EXTENTION_TYPE#project_}s/contrib/${MACHINE_NAME}
    create_patch=1
    # Run rector to see if we can fix anything automatically.
    php -d memory_limit=1536M -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/rector process --verbose ${EXTENTION_TYPE#project_}s/contrib/${MACHINE_NAME} &>  ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.rector_out 2>> ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.rector_stderr

    # Disabled for now given the PHP based rector config.
    #cd ${EXTENTION_TYPE#project_}s/contrib/${COMPOSER_NAME}
    #if [ -z "$(git status --porcelain)" ]; then
    #  cd $CHECKOUT_DIR
    #  create_patch=0
    #  php -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/test_error ${MACHINE_NAME}.${VERSION}
    #  test_error_result=$?
    #  if [ $test_error_result -eq 0 ]; then
    #    # Run rector again but without tests, since that failed.
    #    php -d memory_limit=2G -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/rector process --config rector-no-tests.yml --verbose ${EXTENTION_TYPE#project_}s/contrib/${COMPOSER_NAME} &>  ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.rector-no-tests_out 2>> ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.rector-no-tests_stderr
    #  fi
    #fi

    # Check if rector made any changes.
    cd ${EXTENTION_TYPE#project_}s/contrib/${MACHINE_NAME} || exit 1
    if [ -z "$(git status --porcelain)" ]; then
      cd $CHECKOUT_DIR || exit 1
      # No changes by rector, but we still need to check if info is updatable since there could be only
      # info level php file changes.

      # Check if the info file is updateable (only the info file and informational errors is left).
      php -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/info_updatable ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.upgrade_status.pre_rector.xml
      info_updatable_result=$?

      # Check if the composer.json is updateable (only the info file and informational error is left).
      php -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/composer_json_updatable ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.upgrade_status.pre_rector.xml
      composer_json_updatable_result=$?

    else
      # Found uncommitted changes, so we will create a patch!
      cd $CHECKOUT_DIR || exit 1
      create_patch=1
      # Run Upgrade Status again to see what remained after running rector.
      php -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/drush --root=$CHECKOUT_DIR upgrade_status:analyze --format=checkstyle  ${MACHINE_NAME} > ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.upgrade_status.post_rector.xml 2>> ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.upgrade_status_stderr2
      # Check if the info file is updateable (only the info file and informational error is left).
      php -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/info_updatable ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.upgrade_status.post_rector.xml
      info_updatable_result=$?

      # Check if the composer.json is updateable (only the info file and informational error is left).
      php -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/composer_json_updatable ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.upgrade_status.post_rector.xml
      composer_json_updatable_result=$?
    fi

  else
    # Rector was not needed, but we may still be able to fix the info file.
    php -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/info_updatable ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.upgrade_status.pre_rector.xml
    info_updatable_result=$?
    if [ $info_updatable_result -eq 0 ]; then
      # Make sure to have a clean state for diffing later.
      gitCommit ${EXTENTION_TYPE#project_}s/contrib/${MACHINE_NAME}
    fi

    # Check if the composer.json is updateable (only the info file error is left).
    php -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/composer_json_updatable ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.upgrade_status.pre_rector.xml
    composer_json_updatable_result=$?

    if [ $composer_json_updatable_result -eq 0 ]; then
      # Make sure to have a clean state for diffing later.
      gitCommit ${EXTENTION_TYPE#project_}s/contrib/${MACHINE_NAME}
    fi
  fi

  if [ $info_updatable_result -eq 0 ]; then
      # We decided the info file should be updated, so update it now.
      find $CHECKOUT_DIR/${EXTENTION_TYPE#project_}s/contrib/${COMPOSER_NAME} -type f -name "*.info.yml" | while read -r info_file; do

        php -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/update_info ${info_file} ${MACHINE_NAME}.${VERSION}

      done
      create_patch=1
  fi

  if [ $composer_json_updatable_result -eq 0 ]; then
      # We decided the info file should be updated, so update it now.
      php -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR ./vendor/bin/update_composer_json $CHECKOUT_DIR/${EXTENTION_TYPE#project_}s/contrib/${MACHINE_NAME}/composer.json ${MACHINE_NAME}.${VERSION}
      create_patch=1
  fi

  if [ $create_patch -eq 1 ]; then
    cd ${EXTENTION_TYPE#project_}s/contrib/${COMPOSER_NAME} || exit 1
    # Undo some unrelated changes that rector used to make. We may not need this anymore.
    # php -d sys_temp_dir=$CHECKOUT_DIR -d upload_tmp_dir=$CHECKOUT_DIR $CHECKOUT_DIR/vendor/bin/restore_nonrector_changes $CHECKOUT_DIR/${EXTENTION_TYPE#project_}s/contrib/${MACHINE_NAME}

    git log > ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.git_log.txt
    git status > ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.git_status.txt

    # CREATE THE PATCH!
    git diff > ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.rector.patch

    # Delete the file if it is empty.
    find ${WORKSPACE_DIR}/phpstan-results/${MACHINE_NAME}/${MACHINE_NAME}.${VERSION}.rector.patch -size  0 -print -delete

    cd $CHECKOUT_DIR || exit 1
  fi

fi

# Clean up this workspace for the next project.
git reset --hard HEAD
sudo git clean -ffd

>&2 echo -e "\e[35mDone analyzing ${1}\e[0m"
