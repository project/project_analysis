<?php

namespace InfoUpdater;

use Composer\Semver\Semver;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Response;

class ProcessProjectsFile {

  /**
   * @var \GuzzleHttp\Client
   */
  private Client $client;

  /**
   * @var array|mixed
   */
  protected array $projects;

  public function __construct() {
    $config = [];
    if (getenv('UPDATEBOT_USER_AGENT')) {
      $config = [
        'headers' => [
          'User-Agent' => getenv('UPDATEBOT_USER_AGENT'),
        ],
      ];
    }
    $this->client = new Client($config);
  }

  public function process($projectsFile, $drupalVersion, $artifactsPath = './artifacts') {
    $artifactsPath = rtrim($artifactsPath, '/');
    $this->projects = [];
    $project_tsv_rows = explode("\n", file_get_contents($projectsFile));

    foreach ($project_tsv_rows as $index => $project_tsv_row) {
      $project_tsv_row_data = explode("\t", $project_tsv_row);

      $machineName = $project_tsv_row_data[0];
      if ($machineName !== '') {
        if (isset($this->projects[$machineName])) {
          // row 4 is usage column.
          $project_tsv_row_data[4] = max($project_tsv_row_data[4], $this->projects[$machineName][4]);
        }
        $this->projects[$machineName] = $project_tsv_row_data;
      }
    }


    $urlPattern = 'https://updates.drupal.org/release-history/%s/current';
    $outputFilename = 'updates.xml';
    $this->preloadUrlData($artifactsPath, $urlPattern, $outputFilename);

    $processedProjects = [];
    foreach ($this->projects as $project => $tsv_data) {
      $version = $this->getAvailableVersion($drupalVersion, $project, $artifactsPath);

      if ($version === FALSE) {
        echo PHP_EOL . "$project: No compatible version found" . PHP_EOL;
        // Remove the directory and updates.xml
        $updates_xml = $artifactsPath . '/' . $project . '/updates.xml';
        if (file_exists($updates_xml)) {
          unlink($updates_xml);
          rmdir($artifactsPath . '/' . $project);
        }
        continue;
      }

      $processedProjects[$project] = $tsv_data;
      $processedProjects[$project][2] = (string) $version;
      $processedProjects[$project][8] = $tsv_data[2];

    }

    // Write each project to a TSV file.
    $tsv = '';
    foreach ($processedProjects as $project => $tsv_data) {
      $tsv .= implode("\t", $tsv_data) . PHP_EOL;
    }
    file_put_contents(str_replace('.tsv', '.' . $drupalVersion . '.tsv', $projectsFile), $tsv);

  }

  /**
   * @param $artifactsPath
   *
   * @return void
   */
  public function preloadUrlData($artifactsPath, $urlPattern, $outputFilename): void {
    $client = $this->client;
    $projects = $this->projects;
    print PHP_EOL . '>>>>>>> Processing ' . $outputFilename . PHP_EOL;

    $requests = function() use ($client, $projects, $urlPattern, $artifactsPath, $outputFilename) {
      foreach ($projects as $project => $data) {
        yield function() use ($project, $client, $urlPattern) {
          return $client->getAsync(sprintf($urlPattern, $project));
        };
      }
    };

    $projectIndex = array_keys($projects);
    $pool = new Pool($this->client, $requests(), [
      'concurrency' => 20,
      'fulfilled' => function(Response $response, $index) use ($artifactsPath, $outputFilename, $projectIndex) {
        $contents = $response->getBody()->getContents();
        $local_filename = $artifactsPath . '/' . (string) $projectIndex[$index] . '/' . $outputFilename;
        @mkdir(dirname($local_filename), 0777, TRUE);
        file_put_contents($local_filename, $contents);

        if ($index % 500 === 0) {
          echo $index . '...' . PHP_EOL;
        }
      },
    ]);

    // Initiate the transfers and create a promise
    $promise = $pool->promise();

    // Force the pool of requests to complete.
    $promise->wait();
  }

  private function getAvailableVersion($drupalVersion, $project, $artifactsPath) {
    if (!file_exists($artifactsPath . '/' . $project . '/updates.xml')) {
      throw new \Exception("No updates.xml for $project");
    }

    $update_xml = simplexml_load_file($artifactsPath . '/' . $project . '/updates.xml');

    $highest_level = 'none';
    $highest_compatible = false;
    if (empty($update_xml->releases->release)) {
      echo PHP_EOL . "$project: Has no releases in updates XML" . PHP_EOL;
      return $highest_compatible;
    }
    foreach($update_xml->releases->release as $release) {
      try {
        // This is a compatible release (roughly).
        if (!empty($release->core_compatibility) && Semver::satisfies($drupalVersion, $release->core_compatibility)) {
          if (preg_match('!-dev$!', $release->version) && $highest_level == 'none') {
            $highest_level = 'dev';
            $highest_compatible = $release->version;
          }
          if (preg_match('!-alpha\d+$!', $release->version) && in_array($highest_level, ['none', 'dev'])) {
            $highest_level = 'alpha';
            $highest_compatible = $release->version;
          }
          elseif (preg_match('!-beta\d+$!', $release->version) && in_array($highest_level, ['none', 'dev', 'alpha'])) {
            $highest_level = 'beta';
            $highest_compatible = $release->version;
          }
          elseif (preg_match('!-(RC|rc)\d+$!', $release->version) && in_array($highest_level, ['none', 'dev', 'alpha', 'beta'])) {
            $highest_level = 'RC';
            $highest_compatible = $release->version;
          }
          elseif (preg_match('!\.\d+$!', $release->version) && in_array($highest_level, ['none', 'dev', 'alpha', 'beta', 'RC'])) {
            $highest_level = 'stable';
            $highest_compatible = $release->version;
          }
        }
      }
      catch (\Exception) {
        // There was an invalid version constraint. We'll take the highest
        // version identified so far and hope for the best.
        echo PHP_EOL . "Unparsable constaint for $project: " . htmlentities($release->core_compatibility, 0, 'utf-8') . PHP_EOL;
      }
    }
    return $highest_compatible;
  }

  /**
   * Unused but useful to verify TSV data
   */
  private function getHighestDevVersion(string $projectVersion, $project, $artifactsPath) {
    if (!file_exists($artifactsPath . '/' . $project . '/updates.xml')) {
      throw new \Exception("No updates.xml for $project");
    }

    $update_xml = simplexml_load_file($artifactsPath . '/' . $project . '/updates.xml');

    $highest_level = 'none';
    $highest_compatible = false;
    if (empty($update_xml->releases->release)) {
      echo PHP_EOL . "$project: Has no releases in updates XML" . PHP_EOL;
      return $highest_compatible;
    }

    // Sort all releases by date. Sometimes projects would have 1.6.x available but we need to use 1.x, sometimes project have 1.1.x available and we do need to use 1.1.x, this means best we can do it sort by release date. If 1.x is more up to date, use that (eq acquia_cms_article), if 1.6.x is more up to date, use that (eg webform).
    $releases = ((array) $update_xml->releases)['release'];
    if ($releases instanceof \SimpleXMLElement) {
      $releases = [$releases];
    }
    usort($releases, function($a, $b) {
      return (int) $b->date <=> (int) $a->date;
    });

    $prefixes = [
      '8.x-',
      '9.x-',
      '10.x-',
      '11.x-',
    ];

    $projectVersionOriginal = $projectVersion;
    if (str_contains($projectVersion, 'alpha')){
      // Remove anything like -alpha, -alpha1, -alpha3, -alpha4, etc.
      $projectVersion = preg_replace('!-alpha\d*!', '', $projectVersion);
    }
    if (str_contains($projectVersion, 'beta')){
      // Remove anything like -alpha, -alpha1, -alpha3, -alpha4, etc.
      $projectVersion = preg_replace('!-beta\d*!', '', $projectVersion);
    }
    if (str_contains($projectVersion, 'RC')){
      // Remove anything like -alpha, -alpha1, -alpha3, -alpha4, etc.
      $projectVersion = preg_replace('!-RC\d*!', '', $projectVersion);
    }
    if (str_contains($projectVersion, 'rc')){
      // Remove anything like -alpha, -alpha1, -alpha3, -alpha4, etc.
      $projectVersion = preg_replace('!-rc\d*!', '', $projectVersion);
    }

    $possibleVersions = [
      (string) $projectVersionOriginal,
      (string) $projectVersion,
      substr((string) $projectVersion, 0, strlen((string) $projectVersion) - 1)  . 'x',
      substr((string) $projectVersion, 0, strlen((string) $projectVersion) - 3)  . 'x',
    ];


    foreach($possibleVersions as $projectVersion) {
      foreach ($releases as $release) {
        if ($highest_level !== 'none') {
          continue;
        }

        try {
          // This is a compatible release (roughly).
          $normalizedVersion = str_replace($prefixes, '', $release->version);
          if(strpos('-dev', $normalizedVersion) !== false && $normalizedVersion === $release->version) {
            $highest_level = 'dev';
            $highest_compatible = $release->version;
            continue;
          }

          if (!empty($release->version) && Semver::satisfies($normalizedVersion, $projectVersion)) {
            if (preg_match('!-dev$!', $normalizedVersion) && $highest_level === 'none') {
              $highest_level = 'dev';
              $highest_compatible = $release->version;
            }
          }
        }
        catch (\Exception $e) {
          // Do nothing if it fails, keep trying down the line.
        }
      }
    }

    return $highest_compatible;
  }

}

