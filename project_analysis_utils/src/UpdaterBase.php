<?php

namespace InfoUpdater;

use Composer\Semver\Semver;

abstract class UpdaterBase extends ResultProcessorBase {

  /**
   * Gets the error and warning messages for an upgrade_status xml file.
   *
   * @param string $project_version
   *
   * @param $pre_or_post
   *
   * @return string
   *
   * @throws \Exception
   */
  public static function getMessages(string $project_version, $pre_or_post): string {
    $pre = new UpdateStatusXmlChecker(self::getUpgradeStatusXML($project_version, $pre_or_post));
    return implode(' -- ', $pre->getMessages('error'))
      . ' -- '
      . implode(' -- ', $pre->getMessages('warning'));
  }

  /**
   * Get the file location of an upgrade_status xml file.
   *
   * @param string $project_version
   * @param $pre_or_post
   *
   * @return string
   * @throws \Exception
   */
  public static function getUpgradeStatusXML(string $project_version, $pre_or_post): string {
    $machine_name = explode('.', $project_version)[0];
    return ResultProcessorBase::getResultsDir() . "/$machine_name/$project_version.upgrade_status.{$pre_or_post}_rector.xml";
  }

  /**
   * Gets the minimum core minor for project version based on detected API changes.
   *
   * @param string $project_version
   *
   * @return int
   *   The minor version either 5, 4, 3, 2, 1 or 0.
   * @throws \Exception
   */
  protected static function getMinimumCoreMinor(string $project_version): int {
    $pre_messages = self::getMessages($project_version, 'pre');
    $post_messages = self::getMessages($project_version, 'post');

    foreach ([5, 4, 3, 2, 1] as $minor) {
      $deprecation_version = "drupal:10.$minor.0";
      if (strpos($pre_messages, $deprecation_version) !== FALSE && strpos($post_messages, $deprecation_version) === FALSE) {
        return $minor;
      }
    }
    return 0;
  }

  /**
   * Generates new composer version constraint based on current requirement and new minimum core minor.
   * 
   * @param int|null $minimum_core_minor
   *   New minimum core minor based on Upgrade Status errors reported (and fixed).
   * @param $current_requirement
   *   The current composer constraint.
   *
   * @return string|null
   *   The new composer constraint.
   */
  public static function getVersionRequirement(?int $minimum_core_minor, $current_requirement): ?string {
    $new_core_version_requirement = NULL;
    if ($minimum_core_minor === 5) {
      if (strpos($current_requirement, '10.5') === FALSE) {
        // If 10.5 is not in core_version_requirement it is likely specifying
        // lower compatibility.
        $new_core_version_requirement = '^10.5 || ^11';
      }
    }
    elseif ($minimum_core_minor === 4) {
      if (strpos($current_requirement, '10.5') === FALSE && strpos($current_requirement, '10.4') === FALSE) {
        // If no version 10.5 or 10.4 then we need to set a version.
        $new_core_version_requirement = '^10.4 || ^11';
      }
    }
    elseif ($minimum_core_minor === 3) {
      if (strpos($current_requirement, '10.5') === FALSE && strpos($current_requirement, '10.4') === FALSE && strpos($current_requirement, '10.3') === FALSE) {
        // If no version 10.5, 10.4 or 10.3 then we need to set a version.
        $new_core_version_requirement = '^10.3 || ^11';
      }
    }
    elseif ($minimum_core_minor === 2) {
      if (strpos($current_requirement, '10.5') === FALSE && strpos($current_requirement, '10.4') === FALSE && strpos($current_requirement, '10.3') === FALSE && strpos($current_requirement, '10.2') === FALSE) {
        // If no version 10.5, 10.4, 10.3 or 10.2 then we need to set a version.
        $new_core_version_requirement = '^10.2 || ^11';
      }
    }
    elseif ($minimum_core_minor === 1) {
      if (strpos($current_requirement, '10.5') === FALSE && strpos($current_requirement, '10.4') === FALSE && strpos($current_requirement, '10.3') === FALSE && strpos($current_requirement, '10.2') === FALSE && strpos($current_requirement, '10.1') === FALSE) {
        // If no version 10.5, 10.4, 10.3, 10.2 or 10.1 then we need to set a version.
        $new_core_version_requirement = '^10.1 || ^11';
      }
    } elseif ($minimum_core_minor === 0) {
      if (strpos($current_requirement, '10.5') === FALSE && strpos($current_requirement, '10.4') === FALSE && strpos($current_requirement, '10.3') === FALSE && strpos($current_requirement, '10.2') === FALSE && strpos($current_requirement, '10.1') === FALSE) {
          // If no version 10.5, 10.4, 10.3, 10.2 or 10.1 then we need to set a version.
          $new_core_version_requirement = '^10.1 || ^11';
      }
    }

    // Only update if doesn't already satisfy 10.0.0, will only happen if $minimum_core_minor was NULL.
    if (empty($new_core_version_requirement) && !Semver::satisfies('11.0.0', $current_requirement)) {
      if (!Semver::satisfies('10.5', $current_requirement)) {
        $current_requirement = $current_requirement . ' || ^10.1';
      }

      $new_core_version_requirement = $current_requirement . ' || ^11';
    }
    return $new_core_version_requirement;
  }
}
