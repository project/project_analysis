<?php

namespace InfoUpdater\Tests\Unit;

use InfoUpdater\InfoUpdater;
use InfoUpdater\Tests\Core\InfoParserDynamic;
use InfoUpdater\Tests\TestBase;
use Symfony\Component\Yaml\Yaml;

/**
 * @coversDefaultClass \InfoUpdater\InfoUpdater
 */
class InfoUpdaterTest extends TestBase {

  /**
   * @covers ::updateInfo
   *
   * @dataProvider providerUpdateInfoNew
   */
  public function testUpdateInfoNew($file, $project_version, $expected) {
    $temp_file = $this->createTempFixtureFile($file);
    $pre_yml = Yaml::parseFile($temp_file);

    InfoUpdater::updateInfo($temp_file, $project_version);
    $post_yml = Yaml::parseFile($temp_file);
    $this->assertSame($expected, $post_yml['core_version_requirement']);

    // The created info file should be able to be parsed by the core parser.
    $core_parser = new InfoParserDynamic();
    $core_info = $core_parser->parse($temp_file);
    $this->assertSame($post_yml, $core_info);
    $this->assertArrayNotHasKey('core', $post_yml);

    $pre_yml['core_version_requirement'] = $expected;
    $pre_yml = asort($pre_yml);
    $post_yml = asort($post_yml);
    $this->assertSame($post_yml, $post_yml);

    unlink($temp_file);
  }

  public function providerUpdateInfoNew() {
    return [
      '^10 in core version requirement' => [
        'core_version_requirement_10.info.yml',
        'module_no_errors.1.x-dev',
        '^10 || ^11',
      ],
      '^10 || ^11 already in core version requirement' => [
        'core_version_requirement_10_11.info.yml',
        'module_no_errors.1.x-dev',
        '^10 || ^11',
      ],
      '^10.1 in core version requirement' => [
        'core_version_requirement_1010.info.yml',
        'module_no_errors.1.x-dev',
        '^10.1.0 || ^11',
      ],
      '^10 deprecations with ^10.2 in core version requirement ' => [
        'core_version_requirement_1020.info.yml',
        'module_no_errors.1.x-dev',
        '^10.2.0 || ^11',
      ],
      '^10 deprecations with ^10.3 in core version requirement' => [
        'core_version_requirement_1030.info.yml',
        'module_no_errors.1.x-dev',
        '^10.3.0 || ^11',
      ],
      '^10 deprecations with ^10.4 in core version requirement' => [
        'core_version_requirement_1040.info.yml',
        'module_no_errors.1.x-dev',
        '^10.4.0 || ^11',
      ],
      '^10 deprecations with ^10.5 in core version requirement' => [
        'core_version_requirement_1050.info.yml',
        'environment_indicator.3.x-dev',
        '^10.5.0 || ^11',
      ],
      '10.0.0 deprecations with ^10 in core version requirement - 10.1 because of DeprecationHelper' => [
        'core_version_requirement_10.info.yml',
        'module_10_1_deprecation.1.x-dev',
        '^10.1 || ^11',
      ],
      '10.1.0 deprecations with ^10 || ^11 in core version requirement' => [
        'core_version_requirement_10.info.yml',
        'module_10_1_deprecation.1.x-dev',
        '^10.1 || ^11',
      ],
      '10.1.0 deprecations with ^10.1.0 in core version requirement' => [
        'core_version_requirement_1010.info.yml',
        'module_10_1_deprecation.1.x-dev',
        '^10.1.0 || ^11',
      ],
      '10.1.0 deprecations with ^10.2.0 in core version requirement' => [
        'core_version_requirement_1020.info.yml',
        'module_10_1_deprecation.1.x-dev',
        '^10.2.0 || ^11',
      ],
      '10.1.0 deprecations with ^10.3.0 in core version requirement' => [
        'core_version_requirement_1030.info.yml',
        'module_10_1_deprecation.1.x-dev',
        '^10.3.0 || ^11',
      ],
      '10.1.0 deprecations with ^10.4.0 in core version requirement' => [
        'core_version_requirement_1040.info.yml',
        'module_10_1_deprecation.1.x-dev',
        '^10.4.0 || ^11',
      ],
      '10.1.0 deprecations with ^10.5.0 in core version requirement' => [
        'core_version_requirement_1050.info.yml',
        'module_10_1_deprecation.1.x-dev',
        '^10.5.0 || ^11',
      ],
    ];
  }

  /**
   * @param $file
   *
   * @return string
   */
  protected function createTempFixtureFile($file): string {
    $fixture_file = static::FIXTURE_DIR . "/$file";
    $temp_file = sys_get_temp_dir() . "/$file";
    if (file_exists($temp_file)) {
      unlink($temp_file);
    }
    copy($fixture_file, $temp_file);
    return $temp_file;
  }

}
