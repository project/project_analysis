<?php

namespace InfoUpdater\Tests\Unit;

use InfoUpdater\ProcessProjectsFile;
use PHPUnit\Framework\TestCase;

class ProcessProjectsFileTest extends TestCase {

  public function testProcess() {
    $process = new ProcessProjectsFile();

    $process->process(__DIR__ . '/../../project_list_files/allprojects.tsv', '10.0.0', __DIR__ . '/../../artifacts/');

  }

}
