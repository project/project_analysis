<?php

namespace InfoUpdater\Tests\Unit;

use Composer\Json\JsonFile;
use InfoUpdater\ComposerJsonUpdater;
use InfoUpdater\InfoUpdater;
use InfoUpdater\Tests\Core\InfoParserDynamic;
use InfoUpdater\Tests\TestBase;
use Symfony\Component\Yaml\Yaml;

/**
 * @coversDefaultClass \InfoUpdater\ComposerJsonUpdater
 */
class ComposerJsonUpdaterTest extends TestBase {

  /**
   * @covers ::update
   *
   * @dataProvider providerUpdateComposerJson
   */
  public function testUpdateComposerJson($file, $project_version, $expected) {
    $temp_file = $this->createTempFixtureFile($file);
    $pre_json_file = new JsonFile($temp_file);
    $pre_json = $pre_json_file->read();

    ComposerJsonUpdater::update($temp_file, $project_version);

    $post_json_file = new JsonFile($temp_file);
    $post_json = $post_json_file->read();

    $pre_json['require']['drupal/core'] = $expected;

    $this->assertSame($pre_json, $post_json);

    unlink($temp_file);
  }

  public function providerUpdateComposerJson() {
    return [
      '^9 || ^10 in core version requirement' => [
        'composer_9_10.json',
        'module_no_errors.1.x-dev',
        '^9 || ^10 || ^11',
      ],
      '^8 || ^9 || ^10 in core version requirements' => [
        'composer_8_9_10.json',
        'module_no_errors.1.x-dev',
        '^8 || ^9 || ^10 || ^11',
      ],
      '^10 in core version requirement' => [
        'composer_10.json',
        'module_no_errors.1.x-dev',
        '^10 || ^11',
      ],
      '^10 || ^11 in core version requirement' => [
        'composer_10_11.json',
        'module_no_errors.1.x-dev',
        '^10 || ^11',
      ],
      '^10.1 deprecations with ^9 || ^10 in core version requirement' => [
        'composer_9_10.json',
        'module_10_1_deprecation.1.x-dev',
        '^10.1 || ^11',
      ],
      '^10.1 deprecations with ^8 || ^9 || ^10 in core version requirements' => [
        'composer_8_9_10.json',
        'module_10_1_deprecation.1.x-dev',
        '^10.1 || ^11',
      ],
      '^10.1 deprecations with ^10 in core version requirement' => [
        'composer_10.json',
        'module_10_1_deprecation.1.x-dev',
        '^10.1 || ^11',
      ],
      '^10.1 deprecations with ^10 || ^11 in core version requirement' => [
        'composer_10_11.json',
        'module_10_1_deprecation.1.x-dev',
        '^10.1 || ^11',
      ],
      '^10.0 deprecations with ^9 || ^10 in core version requirement - 10.1 because of DeprecationHelper' => [
        'composer_9_10.json',
        'module_10_0_deprecation.1.x-dev',
        '^10.1 || ^11',
      ],
      '^10.0 deprecations with ^8 || ^9 || ^10 in core version requirements - 10.1 because of DeprecationHelper' => [
        'composer_8_9_10.json',
        'module_10_0_deprecation.1.x-dev',
        '^10.1 || ^11',
      ],
      '^10.0 deprecations with ^10 in core version requirement - 10.1 because of DeprecationHelper' => [
        'composer_10.json',
        'module_10_0_deprecation.1.x-dev',
        '^10.1 || ^11',
      ],
      '^10.0 deprecations with ^10 || ^11 in core version requirement - 10.1 because of DeprecationHelper' => [
        'composer_10_11.json',
        'module_10_0_deprecation.1.x-dev',
        '^10.1 || ^11',
      ],
    ];
  }

  /**
   * @param $file
   *
   * @return string
   */
  protected function createTempFixtureFile($file): string {
    $fixture_file = static::FIXTURE_DIR . "/$file";
    $temp_file = sys_get_temp_dir() . "/$file";
    if (file_exists($temp_file)) {
      unlink($temp_file);
    }
    copy($fixture_file, $temp_file);
    return $temp_file;
  }

}
