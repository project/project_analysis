#!/bin/bash
set -eu

export WORKSPACE_DIR="${WORKSPACE_DIR:-/build/drupalci/workspace}"
export CHECKOUT_DIR="${CHECKOUT_DIR:-/build/drupalci/drupal-checkout}"


# This file is intended to be executed on the testbot docker container.
export PHPSTAN_RESULT_DIR="${WORKSPACE_DIR}/phpstan-results"

# This rector.php is needed so autoloading doesnt break
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cp $SCRIPT_DIR/rector.php ${CHECKOUT_DIR}
git -C ${CHECKOUT_DIR} add .
git -C ${CHECKOUT_DIR} commit -q -m "Add new rector.php configuration"

export COMPOSER_HOME=${WORKSPACE_DIR}/composer-home


# Prepare debug patch
composer --working-dir=${CHECKOUT_DIR} config --no-plugins allow-plugins.cweagans/composer-patches true
composer --working-dir=${CHECKOUT_DIR} require cweagans/composer-patches --no-interaction --no-progress
cp $SCRIPT_DIR/patches.json ${CHECKOUT_DIR}

composer --working-dir=${CHECKOUT_DIR} config extra.patches-file "./patches.json"

git -C ${CHECKOUT_DIR} add .
git -C ${CHECKOUT_DIR} commit -q -m "More debug information"

# Update the drupal-composer.lock.json
cp ${CHECKOUT_DIR}/composer.lock ${WORKSPACE_DIR}/phpstan-results/drupal-composer.lock.json

composer --working-dir=${CHECKOUT_DIR} remove drupalorg_infrastructure/project_analysis_utils
composer --working-dir=${CHECKOUT_DIR} require drupalorg_infrastructure/project_analysis_utils

git -C ${CHECKOUT_DIR} add .
git -C ${CHECKOUT_DIR} commit -q -m "Update project analysis internal library"

composer --working-dir=${CHECKOUT_DIR} config prefer-stable false
git -C ${CHECKOUT_DIR} add .
git -C ${CHECKOUT_DIR} commit -q -m "Don't prefer stable here"

# Set up as many workspaces for running PHPStan as many processor cores we have.
PROC_COUNT=4
echo -e "\e[35mPreparing ${PROC_COUNT} workspaces\e[0m"
parallel ./prepare_workspace.sh {} ::: $(seq -s' ' 1 ${PROC_COUNT})

# Run the analyzers in parallel in each workspace we created.
# 1/2/3/4/9 correspond to the columns in the project listing file
echo -e "\e[35mStarting analysis with ${PROC_COUNT} threads\e[0m"
/usr/bin/time -v parallel -j${PROC_COUNT} --colsep '\t' --timeout 900 ./analyzer.sh "{1}" "{2}" "{3}" "{4}" "{%}" "{9}" :::: ${WORKSPACE_DIR}/projects.tsv 2>&1 > ${WORKSPACE_DIR}/analyzer_output.log
