<?php

use RectorIssues\DataBase;
use RectorIssues\DrupalOrg\PatchInspector;

require_once __DIR__ . '/vendor/autoload.php';
$results_dir = 'phpstan_run/project_analysis_results/results/phpstan-results';
$files = glob($results_dir . '/*/*.rector.patch');
$dest = 'invalid_patches/';
foreach ($files as $file) {
  if (strpos($file, 'bootstrap_horizontal_tabs.2.0.1.rector.patch') !== FALSE) {
    $A = 'A';
  }
  $p = new PatchInspector($file);
  if ($p->isOnlyUseStatements()) {
    $pathParts = pathinfo($file);
    copy($file, $dest . $pathParts['filename']);
  }


}

