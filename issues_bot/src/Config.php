<?php

namespace RectorIssues;

/**
 * Configuration settings.
 *
 * The config.yml is committed to the repo.
 */
class Config extends SettingsBase {

  public const FILE = 'config.yml';
}
