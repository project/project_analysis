<?php

namespace RectorIssues\DrupalOrg;

use RectorIssues\Config;
use RectorIssues\DataBase;
use RectorIssues\Settings;
use RectorIssues\UtilsTrait;

/**
 * Class for dealing with drupal.org projects.
 *
 * Information about patch/issue status is kept in database to avoid extra calls
 * to the REST API for each run.
 */
class Project {

  use UtilsTrait;

  /**
   * @var array
   */
  protected $projectRecord;

  /**
   * @var string
   */
  protected $updateXML;

  /**
   * @var string
   */
  private $version;

  /**
   * @var string
   */
  private $name;

  /**
   * @var \RectorIssues\DataBase
   */
  private $db;

  /**
   * @var string
   */
  private $rectorStatus;

  /**
   * @var int
   */
  private $rectorIssue;

  /**
   * Project as return by drupal.org REST API.
   *
   * @var \stdClass
   */
  private $drupalOrgProject;

  /**
   * Project nid.
   *
   * @var int
   */
  private $nid;

  /**
   * Statuses for projects.
   */
  const STATUS_OTHER_UNKNOWN = 'STATUS_OTHER_UNKNOWN';
  const STATUS_OTHER_ISSUE = 'STATUS_OTHER_ISSUE';
  const STATUS_ISSUE_OPEN = 'STATUS_ISSUE_OPEN';
  //const STATUS_NO_RELEASES = 'STATUS_NO_RELEASES';
  const STATUS_NOT_MAINTAINED = 'STATUS_NOT_MAINTAINED';
  const STATUS_INVALID_PROJECT = 'STATUS_INVALID_PROJECT';

  /**
   * Project constructor.
   *
   * @param $name
   * @param $version
   *
   * @throws \Envms\FluentPDO\Exception
   */
  public function __construct($name, $version) {
    $this->name = $name;
    $this->version = $version;
    $this->db = DataBase::getInstance();
    $this->projectRecord = $this->getProjectRecord();
    $this->setIssueInfoFromRecord();
  }

  /**
   * Creates a project from name and version number.
   *
   * @param string $short_name
   * @param string $version
   *
   * @return static
   */
  public static function createFromName($short_name, $version) {
    return new static(
      $short_name,
      $version
    );
  }

  /**
   * Gets the rector issue.
   *
   * @return int|null
   */
  public function getRectorIssue() {
    if (!$this->rectorIssue){
      $issue = $this->findIssue();
      if ($issue) {
        $this->rectorIssue = $issue->nid;
      }
    }

    return $this->rectorIssue;
  }

  /**
   * Sets issue info from record.
   *
   * @return bool
   */
  private function setIssueInfoFromRecord() {
    if ($this->projectRecord) {
      $this->rectorStatus = $this->projectRecord['rector_status'];
      $this->rectorIssue = $this->projectRecord['rector_issue'];
      $this->nid = $this->projectRecord['nid'];
      return TRUE;
    }
    $this->rectorStatus = static::STATUS_OTHER_UNKNOWN;
    return FALSE;
  }

  /**
   * Determines if a project should be skipped.
   *
   * @param int $build_timestamp
   *
   * @return bool
   */
  public function skip(int $build_timestamp) {
    $skip_status = in_array($this->rectorStatus,
      [
        static::STATUS_OTHER_ISSUE,
        //static::STATUS_NO_RELEASES,
        static::STATUS_NOT_MAINTAINED,
      ]
    );
    if ($skip_status) {
      return TRUE;
    }
    elseif (!Settings::getSetting('check_last_release') || strpos($this->version, '.x') === FALSE) {
      return FALSE;
    }
    // Check the latest release time.
    $xml = $this->getUpdateXML();
    if (empty($xml)) {
      return TRUE;
    }
    else {
      $version_pattern = preg_quote('8.x-' . $this->version);
      $release_time = static::getReleaseTime($xml, $version_pattern);
      if (empty($release_time)) {
        return FALSE;
      }
      return $release_time > $build_timestamp;
    }
  }

  /**
   * Gets the time of the release.
   *
   * @param \SimpleXMLElement $xml
   *   The XML that contains the releases
   * @param string $pattern
   *   The regex pattern to match.
   *
   * @return int|null
   */
  private static function getReleaseTime(\SimpleXMLElement $xml, string $pattern) {
    if ($release = static::getXMLRelease($xml, $pattern)) {
      return (int) $release->date;
    }
    return NULL;
  }

  /**
   * Find the drupal.org issue based on the drupoal.org REST API.
   */
  public function findIssue() {
    // Attempt to find the issue based on the author, project node ID and version.
    $uid = Settings::getRequiredSetting('uid');
    $tid = Config::getSetting('bot_tag_tid');

    $this->debug("Looking for issue for {$this->name} {$this->version}");
    $issues = Request::getAllPages(
      "/api-d7/node.json?type=project_issue&author=" . $uid . "&field_project=" . $this->nid . '&field_issue_version=' . $this->version . '&taxonomy_vocabulary_9=' . (int) Config::getSetting('bot_tag_tid'),
      ['title', 'nid', 'status', 'taxonomy_vocabulary_9', 'field_issue_version']
    );
    $this->debug("Found " . count($issues) . " issues for {$this->name} {$this->version}");

    $prefixes = [
      '8.x-',
      '9.x-',
      '10.x-',
      '11.x-',
    ];

    // Look for all issues and compare field_issue_version to the version. Mostly to handle the move to dev versions.
    if (count($issues) === 0 && str_contains($this->version, 'x')) {
      $normalized_version = str_replace('.x-dev', '.', $this->version);
      $normalized_version = str_replace('.x', '.', $normalized_version);
      $this->debug("Looking for issue for {$this->name} without version");
      $issuesSet = Request::getAllPages(
        "/api-d7/node.json?type=project_issue&author=" . $uid . "&field_project=" . $this->nid . '&taxonomy_vocabulary_9=' . (int) Config::getSetting('bot_tag_tid'),
        ['title', 'nid', 'status', 'taxonomy_vocabulary_9', 'field_issue_version']
      );


      $issues = [];
      foreach ($issuesSet as $issue) {
        if ($issue->field_issue_version == $this->version) {
          $this->debug('Found with proper version ' . $issue->field_issue_version . ' == ' . $this->version);
          $issues[] = $issue;
          continue;
        }

        if (str_starts_with($issue->field_issue_version, $normalized_version) !== FALSE) {
          $this->debug('Found with proper version ' . $issue->field_issue_version . ' starts with ' . $normalized_version);
          $issues[] = $issue;
          continue;
        }

        foreach ($prefixes as $prefix) {
          if (str_starts_with(preg_replace('/-(alpha|beta|rc).*/', '', $issue->field_issue_version), $prefix . $normalized_version) !== FALSE) {
            $this->debug('Found with proper version ' . $issue->field_issue_version . ' starts with ' . $prefix . $normalized_version);
            $issues[] = $issue;
            break;
          }
        }

      }
      $this->debug("Found " . count($issues) . " issues for {$this->name} with fuzzy search");
    }  else {
      // When no .x version and direct version is found, lets try with prefixes.
      $this->debug("Looking for issue for {$this->name} without version and without version ending with .x");
      $issuesSet = Request::getAllPages(
        "/api-d7/node.json?type=project_issue&author=" . $uid . "&field_project=" . $this->nid . '&taxonomy_vocabulary_9=' . (int) Config::getSetting('bot_tag_tid'),
        ['title', 'nid', 'status', 'taxonomy_vocabulary_9', 'field_issue_version']
      );

      $issues = [];
      foreach ($issuesSet as $issue) {
        if ($issue->field_issue_version == $this->version) {
          $this->debug('Found with proper version ' . $issue->field_issue_version . ' == ' . $this->version);
          $issues[] = $issue;
          continue;
        }

        foreach ($prefixes as $prefix) {
          if (str_starts_with(preg_replace('/-(alpha|beta|rc).*/', '', $issue->field_issue_version), $prefix . $this->version) !== FALSE) {
            $this->debug('Found with proper version ' . $issue->field_issue_version . ' starts with ' . $prefix . $this->version);
            $issues[] = $issue;
            break;
          }
        }

      }
      $this->debug("Found " . count($issues) . " issues for {$this->name} with fuzzy search against non .x version.");
    }

    $issues = array_reverse($issues);
    foreach ($issues as $issue) {
      foreach ($issue->taxonomy_vocabulary_9 as $term) {
        // ProjectUpdateBotD11
        if ((int) $term->id === $tid) {
          $this->debug('Found with proper tag and version ' . $issue->field_issue_version . ', target version ' . $this->version);
          return $issue;
        }
      }
    }
    $this->debug('Did not find issue with proper tag and version');
    return NULL;
  }

  /**
   * Gets the project record.
   *
   * @return null|array
   * @throws \Envms\FluentPDO\Exception
   */
  private function getProjectRecord() {
    $query = $this->db->from('projects')
      ->where('name', $this->name)
      ->where('version', $this->version);
    if ($query->count() > 1) {
      throw new \Exception("should have never have more than 1 project/version row {$this->name} : {$this->version}");
    }
    elseif ($query->count() === 0) {
      return NULL;
    }
    return DataBase::castColumns($query->fetchAll())[0];
  }

  /**
   * Gets a single record column value.
   *
   * @param $column
   *
   * @return mixed|null
   * @throws \Envms\FluentPDO\Exception
   */
  public function getRecordColumn($column) {
    if ($record = $this->getProjectRecord()) {
      return $record[$column];
    }
    return NULL;
  }

  /**
   * Sets the project record in the database.
   *
   * @param array $record
   *
   * @throws \Envms\FluentPDO\Exception
   */
  public function setRecord(array $record) {
    if (empty($record['nid'])) {
      if (!empty($this->nid)) {
        $record['nid'] = $this->nid;
      }
      else {
        $record['nid'] = $this->getProjectNidFromDrupalOrg();
      }
    }
    $record['version'] = $record['version'] ?? $this->version;
    $record['name'] = $record['name'] ?? $this->name;
    if ($existingRecord = $this->getProjectRecord()) {
      $update = $this->db
        ->update('projects')
        ->set($record)
        ->where('nid', $this->nid)
        ->where('version', $this->version)
        ->execute();
      if ($update !== 1) {
        new \Exception("could not UPDATE");
      }
    }
    else {
      $insert = $this->db->insertInto('projects', $record)->execute();
      if ($insert === FALSE) {
        throw new \Exception("could not insert");
      }
    }
    $this->projectRecord = $record;
  }

  /**
   * Updates the project record if needed.
   *
   * @throws \Envms\FluentPDO\Exception
   */
  public function updateRecord() {
    $record = $this->projectRecord;
    $update_record = FALSE;
    if (empty($record['nid'])) {
      $record['rector_status'] = $this->rectorStatus;
      $nid = $this->getProjectNidFromDrupalOrg();
      if (!$nid) {
        return;
      }
      $record['nid'] = $nid;
      $update_record = TRUE;
    }
    $needs_status_check = in_array(
      $this->rectorStatus,
      [
        static::STATUS_OTHER_UNKNOWN,
        static::STATUS_NOT_MAINTAINED,
        //static::STATUS_NO_RELEASES,
      ]
    );
    if ($needs_status_check) {
      /*if (!$this->hasDrupal8Release()) {
        $record['rector_status'] = static::STATUS_NO_RELEASES;
      }*/
      if (!$this->isMaintained()) {
        $record['rector_status'] = static::STATUS_NOT_MAINTAINED;
      }
      // Only perform expensive checks if this is new record.
      elseif (empty($this->projectRecord['nid'])) {
        if ($this->isInvalidProject()) {
          $record['rector_status'] = static::STATUS_INVALID_PROJECT;
        }

      }
      if (empty($this->projectRecord) || $record['rector_status'] !== $this->projectRecord['rector_status']) {
        $update_record = TRUE;
      }
    }
    if ($update_record) {
      $this->setRecord($record);
    }
  }

  /**
   * Get update XML.
   *
   * @return false|\SimpleXMLElement
   */
  private function getUpdateXML() {
    if (!isset($this->updateXML)) {
      try {
        $this->updateXML = new \SimpleXMLElement(file_get_contents("https://updates.drupal.org/release-history/{$this->name}/current"));
      }
      catch (\Exception $exception) {
        $this->logError("Could not get xml for {$this->name}", $exception);
        return FALSE;
      }
    }
    return $this->updateXML;
  }

  /**
   * Determine if the project has any Drupal 8 releases.
   *
   * @return bool
   */
  /*private function hasDrupal8Release() {
    if ($xml = $this->getUpdateXML()) {
      return !empty($this->getXMLRelease($xml, '8\.x-.*'));
    }
    return FALSE;
  }*/

  /**
   * Gets a release XML object.
   *
   * @param $xml
   *   The parent XML.
   * @param $pattern
   *   The version pattern ot match.
   *
   * @return mixed|null
   *   The release XML or null if there ar no matches.
   */
  private static function getXMLRelease($xml, $pattern) {
    foreach ($xml->releases[0] as $release) {
      if ($release->version != NULL) {
        $version = (string) $release->version;
        // @todo should we require a now dev releases.
        if (preg_match("/$pattern/", $version) === 1) {
          return $release;
        }
      }
    }
    return NULL;
  }

  /**
   * Determines if a project is currently supported.
   *
   * @return bool
   */
  private function isMaintained() {
    if ($xml = $this->getUpdateXML()) {
      $supported = FALSE;
      $developed = FALSE;
      foreach ($xml->terms[0] as $term) {
        if ($term->name != NULL) {
          $term_name = (string) $term->name;
          $term_value = (string) $term->value;
          if ($term_name === 'Maintenance status') {
            if ($term_value === 'Unsupported') {
              return FALSE;
            }
            $supported = TRUE;
          }
          elseif ($term_name === 'Development status') {
            if ($term_value === 'Obsolete') {
              return FALSE;
            }
            $developed = TRUE;
          }
        }
      }
      return $supported && $developed;
    }
    return FALSE;
  }

  /**
   * Gets the nid for the project.
   *
   * @return int
   */
  private function getProjectNidFromDrupalOrg() {
    if (empty($this->nid)) {
      // First attempt to get the project via the REST API.
      $project = $this->getDrupalOrgProject();
      if (!$project) {
        // Sometimes d.o return 5xx when requesting some projects.
        // As back get the project nid from visiting the actual project page.
        // The class 'page-node-[nid]' should be on all project pages. Use this
        // to get the nid.
        $contents = static::getDrupalOrgFile(static::getSiteUrl() . '/project/' . $this->name);
        $matches = [];
        if (preg_match('/page-node-[0-9]+/', $contents, $matches)) {
          if (count($matches) === 1) {
            $this->nid = (int)(explode('-', $matches[0])[2]);
          }
        }
      }
      else {
        $this->nid = (int)$project->nid;
      }
    }
    return $this->nid;
  }

  /**
   * Get project via drupal.org REST.
   *
   * @return \stdClass|null
   */
  private function getDrupalOrgProject() {
    if (!isset($this->drupalOrgProject)) {
      foreach (['module', 'theme'] as $project_type) {
        $project = Request::getSingle("/api-d7/node.json?type=project_$project_type&field_project_machine_name=" . $this->name);
        if ($project) {
          $this->drupalOrgProject = $project;
          break;
        }
      }
      if (!isset($this->drupalOrgProject)) {
        $this->logError("No project for {$this->name}");
        return NULL;
      }
    }
    $this->nid = (int) $this->drupalOrgProject->nid;
    return $this->drupalOrgProject;
  }

  /**
   * Determine if a project invalid for providing patches.
   *
   * @return bool
   */
  private function isInvalidProject() {
    $project = $this->getDrupalOrgProject();
    if (!$project) {
      // If couldn't find the project don't mark as invalid. This will make sure
      // another attempt is made.
      return FALSE;
    }
    if (!$project->field_project_has_issue_queue
      || $project->field_project_type !== 'full'
      || !$project->field_project_has_releases
      || !empty($project->field_replaced_by)
    ) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Sets and saves the rector issue.
   *
   * @param int $issue
   */
  public function setRectorIssue(int $issue) {
    $record['rector_issue'] = $issue;
    $record['rector_status'] = static::STATUS_ISSUE_OPEN;
    $this->setRecord($record);
  }
}
