<?php

namespace RectorIssues\DrupalOrg;

use RectorIssues\DataBase;
use RectorIssues\UtilsTrait;

/**
 * Quick hardcoded class to fix summary and tags for issues.
 *
 * In the future this could be used for general issue updates that don't have
 * patches.
 */
class IssueUpdater extends IssueBase {

  use UtilsTrait;

  /**
   * @var \RectorIssues\DataBase
   */
  private $db;

  /**
   * IssueUpdater constructor.
   */
  public function __construct() {
    $this->db = DataBase::getInstance();
  }

  /**
   * Updates all the possible issues.
   *
   * @param array $update_fields
   */
  public function updateIssues() {
    $query = $this->db->from('projects')
      //->where('rector_issue IS NOT ?', null)
      ->where('rector_status', Project::STATUS_ISSUE_OPEN);
    $results = $query->fetchAll();
    foreach (DataBase::castColumns($results) as $row) {
      $issue = $this->getNode($row['rector_issue']);
      if ($this->skipIssue($issue, $row)) {
        continue;
      }
      $this->updateIssue($issue);
      $this->debug("UPdated {$row['name']}.{$row['version']}");
    }
  }

  /**
   * {@inheritdoc}
   */
  protected static function skipIssue($issue, $record = NULL) {
    $result = parent::skipIssue($issue);
    if ($result) {
      return $result;
    }

    $test_projects = [
      'layout_builder_tabs',
      'masquerade',
      'email_registration',
      'sitemeta',
      'entity_reference_number_widget',
      'static_page',
      'toolbar_responsive_search',
      'basic',
      'block_visibility_groups',
      'ctools',
      'codefilter',
      'commerce_notification',
      'commerce_add_to_cart_extras',
      'commerce_ajax_cart',
      'commerce_discount',
      'commerce_discount_extra',
      'commerce_taxcloud',
      'commerce_ups',
      'commerce_yotpo',
      'composer_generate',
      'composer_security_checker',
      'ctools_header_plugins',
      'drupal',
      'editablefields',
      'entityform',
      'formtips',
      'inline_conditions',
      'mathjax',
      'menu_attributes',
      'menu_block',
      'menu_item_visibility',
      'menu_node_views',
      'menu_position',
      'moderation_sidebar',
      'olark',
      'panels',
      'pistachio',
      'resource_conflict',
      'responsive_table_filter',
      'shib_auth',
      'starter_theme',
      'theme_system_sandbox',
      'twig_extensions',
      'twig',
      'twigify',
      'views_ajax_history',
      'views_bulk_operations',
      'views_natural_sort',
      'block_visibility_groups',
      'admin_toolbar',
      'onlyone',
      'asana',
      'base64_image_filter',
      'bulkdelete',
      'contribute',
      'drupal_ipsum',
      'drush_entity',
      'drush_help',
      'entity_field_condition',
      'path_alias_xt',
      'grn',
      'hires_images',
      'ipsum',
      'lazy_config_form',
      'like_and_dislike',
      'modules_weight',
      'moon_phases',
      'no_autocomplete',
      'node_limit',
      'node_revision_bulk_delete',
      'node_revision_cleanup',
      'node_revision_delete',
      'node_revision_restrict',
      'node_revisions_autoclean',
      'revision_deletion',
      'revision_disintegrate',
      'revision_log_default',
      'revision_snip',
      'revisionator',
      'suggest_similar_titles',
      'tattwa_clock',
      'views_oai_pmh',
      'twig_vardumper',
      'filelist_formatter',
      'domain_simple_sitemap',
      'critical_css',
      'commerce_seur',
      'webreader',
      'agov',
      'allocated_seating',
      'bean_panels',
      'bot_beeroclock',
      'better_normalizers',
      'block_access',
      'browser_push_notification',
      'builder_notes',
      'cacheability_metadata_inspector',
      'chatbot_api',
      'commerce_ezypay',
      'commerce_nab_transact',
      'commerce_node_checkout',
      'commerce_order_reference',
      'commerce_pin',
      'commerce_post_affiliate_pro',
      'commerce_recurring',
      'contact_storage',
      'context_chains',
      'country_field',
      'dashboard_connector',
      'dbal',
      'defaultcontent',
      'default_content',
      'api_ai_webhook',
      'ds_chains',
      'domaincontext',
      'drupal',
      'drush_cmi_tools',
      'sar',
      'dynamic_entity_reference',
      'empty_theme',
      'entity_pilot',
      'backfill_formatter',
      'entity_hierarchy',
      'entityform_null_storage',
      'field_collection',
      'field_collection_tabs',
      'field_library',
      'field_redirection',
      'field_union',
      'google_dfp',
      'google_appliance',
      'uc_hotel',
      'imagecache_external',
      'interval',
      'reel',
      'layout_library',
      'ldap',
      'link_attributes',
      'lms',
      'linky',
      'linkyreplacer',
      'linky_revision_ui',
      'media_fiftysix',
      'media_entity_browser',
      'media_godtube',
      'media_myvi',
      'media_rutube',
      'media_video_i_ua',
      'media_video_mail_ru',
      'media_myspace_videos',
      'media_tubeua',
      'media_webcam',
      'microcontent',
      'node_display_field',
      'nsw_feedback',
      'paragraphs_layouts',
      'pnx_gallery',
      'pnx_media',
      'pnx_media_embed',
      'preview_link',
      'publishing_dropbutton',
      'renderviz',
      'reply',
      'reply_to',
      'review_token',
      'robot',
      'script_manager',
      'search_365',
      'search_api_multi_facetapi',
      'search_api_solr_overrides',
      'rest_api_doc',
      'simple_gse_search',
      'slushi_cache',
      'svg_icon',
      'svg_sanitizer',
      'uc_attribute_files',
      'uc_subproduct',
      'ca_catalog_terms',
      'user_contacts',
      'user_files',
      'workbench_email',
      'workbench_moderation',
      'arcgis_webmap',
      'automatic_updates',
      'commerce_installments',
      'commerce_migrate',
      'commerce_related_lines',
      'curbstone_vt_relay',
      'datetime_extras',
      'migrate_drupal_d8',
      'commerce_qb_webconnect',
      'drupal',
      'projectapplications',
      'group_content_menu',
      'htmlpurifier',
      'jvectormap',
      'keyvalue_filestore',
      'mentoring',
      'migrate_plus',
      'migrate_source_csv',
      'migrate_source_ui',
      'migrate_tools',
      'migrate_upgrade',
      'migrate_webform',
      'oauth2_token_manager',
      'prepopulate',
      'redirect_hostname',
      'rules_session_vars',
      'search_api_multi_facetapi',
      'subfields',
      'views_url_path_arguments',
      'user_history',
      's3fs_cors',
      'ultimate_cron_views',
    ];
    if (in_array($record['name'], $test_projects)) {
      return TRUE;
    }
    return FALSE;
  }


  /**
   * Creates a comment for a patch.
   *
   * @param \stdClass $issue
   * @param array $comment_tokens
   *
   * @return bool @bool
   *   TRUE if comment was created.
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  private function updateIssue($issue) {
    $body = $issue->body->value;
    $matches = [];
    $pattern = '/<h3 id="bot-test".*\<p\>/s';
    preg_match($pattern, $body, $matches);
    $tid = Config::getSetting('compatibility_tag_tid');
    $issue_tagged = static::issueHasTag($issue, $tid);
    if (empty($matches) && $issue_tagged) {
      return FALSE;
    }
    $this->visit("/node/$issue->nid");
    $page = $this->getSession()->getPage();
    $update = FALSE;

    if ($matches) {
      $summary_field = $page->findField('edit-body-und-0-value');
      $summary = $summary_field->getValue();
      $pattern = '/<h3 id="bot-test".*project\./s';
      $summary = preg_replace($pattern, '', $summary);
      //$summary = str_replace($matches[0], '', $summary);
      $summary_field->setValue($summary);
      $update = TRUE;
    }

    //$page->fillField('edit-field-issue-status-und', self::ISSUE_STATUS_NEEDS_REVIEW);
    $page->fillField('edit-nodechanges-comment-comment-body-und-0-value', 'Fixing summary');
    $tags_field = $page->findField('edit-taxonomy-vocabulary-9-und');
    if (!$tags_field) {
      throw new \Exception("could not find field edit-taxonomy-vocabulary-9-und");
    }
    $tags = $tags_field->getValue();
    $new_tag = 'Drupal 11 compatibility';
    if (strpos($tags, $new_tag) === FALSE) {
      $tags .= ", Drupal 11 compatibility";
    }

    $tags_field->setValue($tags);

    $page->pressButton('Save');
    $this->debugOutput();
    if (strpos($page->find('css', '.messages.status')->getText(), 'has been updated') === FALSE) {
      $this->logError("Comment could not be created for: $issue");
      return FALSE;
    }
    return TRUE;
  }


}
