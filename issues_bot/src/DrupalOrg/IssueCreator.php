<?php

namespace RectorIssues\DrupalOrg;

use dekor\ArrayToTextTable;
use RectorIssues\Cache;
use RectorIssues\Config;
use RectorIssues\Settings;
use RectorIssues\UtilsTrait;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Defines a class that creates issues and comments on drupal.org
 */
class IssueCreator extends IssueBase {

  use UtilsTrait;

  /**
   * Results from this repo but from a different branch will be cloned here.
   */
  public const RESULTS_DIR = 'phpstan_run/project_analysis_results/results/phpstan-results';

  private int $limit;

  private int $offset;

  /**
   * Create project issues from patches.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Envms\FluentPDO\Exception
   * @throws \Psr\Cache\InvalidArgumentException
   */
  public function createIssues() {
    $this->setUp();

    // By default use the last build from the job repo.
    $build = Settings::getSetting('build_num', 'latest');
    $job_repo = Settings::getRequiredSetting('job_repo');

    $git_dir = static::getBaseDir() . '/phpstan_run/project_analysis_results';
    mkdir($git_dir, 0777, TRUE);
    chdir(static::getBaseDir());


    // Remove results to check it out again.
    $this->debug('Cleaning up results git checkout');
    exec("rm -rf $git_dir");
    $job_branch = Settings::getRequiredSetting('job_branch');

    // Clone the branch which contains the results from the bot run.
    $this->debug("Cloning $job_repo branch $job_branch to $git_dir");
    exec("git clone --depth 5 --branch {$job_branch} {$job_repo} $git_dir", $gitOutput);


    $result = chdir($git_dir);
    $this->debug('Changed to ' . getcwd() . ' result ' . (string) $result);

    // Get latest tag from the current branch.
    $latest_tag = exec('git describe --abbrev=0 --tags');
    $this->debug("Latest tag: $latest_tag");

    $build_info = [
      'build_num' => $latest_tag,
    ];

    // If a specific tag is requested, check that out to a new branch.
    if ($build !== 'latest') {
      exec("git checkout tags/{$build} -b 'results-{$build}'");
      $build_info['build_num'] = $build;
    }

    $build_num = $build_info['build_num'];

    $this->debugFile = static::getBaseDir() . "/debug/$build_num." . date('m-d-Y_hia') . '.txt';

    // Use commit timestamp as build-timestamp since that is easy to get
    $build_timestamp = (int) (exec('git log -1 --format=%at refs/tags/' . $build_num));
    $this->debug("Build timestamp: $build_timestamp");

    chdir(static::getBaseDir());
    $comment_tokens['run'] = $build_num;

    $cache = new Cache();
    if (Settings::isTesting()) {
      $cache->clear();
    }
    /** @var \Symfony\Component\Cache\CacheItem $last_run_cache */
    $last_run_cache = $cache->getItem('last_run');
    $last_run = $last_run_cache->get();
    if (!empty($last_run['phpstan_run'])) {
      if ($last_run['phpstan_run']['id'] === $build_num) {
        $this->debug("Already ran for project_analysis build: $build_num");
        return;
      }
    }

    // Gather information about packages used from the composer.lock file.
    // Adding this to the comments will help in debugging patch problems.
    // To get all the package information you can use the
    // drupal-composer.lock.json that is part of the job artifacts.
    $composer_lock = json_decode(file_get_contents(static::RESULTS_DIR . '/drupal-composer.lock.json'), TRUE);
    $package_names = [
      'mglaman/phpstan-drupal',
      'palantirnet/drupal-rector',
      'rector/rector-prefixed',
      'drupal/upgrade_status'
    ];
    $package_info = '<ol>';
    foreach (array_merge($composer_lock['packages'], $composer_lock['packages-dev']) as $package) {
      $package_name = $package['name'];
      if (in_array($package_name, $package_names)) {
        $package_version = $package['version'];
        $package_info .= "<li>$package_name: $package_version</li>";
        if ($package_name === 'palantirnet/drupal-rector') {
          $comment_tokens['drupal_rector_version'] = $package_version;
        }
      }
    }
    $package_info .= "</ol>";
    $comment_tokens['package_info'] = $package_info;
    $files = glob(static::RESULTS_DIR . '/*/*.rector.patch');
    $supported_projects = [];
    $test_projects = Settings::getSetting('use_test_projects');
    $skipped = 0;
    $current = 0;
    $firstSkipped = FALSE;
    $lastSkipped = FALSE;

    if ($test_projects) {
      $this->debug("Test Projects: " . implode(', ', $test_projects));
    }
    foreach ($files as $file) {
      chdir(static::getBaseDir());

      $ignore_existing_patch = false;
      $project_name = str_replace('.rector.patch', '', basename($file));
      //$short_name = preg_replace('!\\.!', ' ', $project_name, 1);
      [$short_name, $version] = explode('.', $project_name, 2);
      // If we are using test project(s), skip all others.
      if ($test_projects && !in_array($short_name, $test_projects)) {
        // $this->debug("Skipping $project_name ($short_name)"); no need to show skip for test projects.
        continue;
      }

      // Check offset and limits reached.
      if ($this->shouldBreak($current)) {
        break;
      }
      // Skip the ones before and at the limit.
      if ($this->shouldSkipNumber($current++)) {
        if ($firstSkipped === FALSE) {
          $firstSkipped = $short_name;
        }
        $lastSkipped = $short_name;
        continue;
      }

      // Debug skipping once.
      if ($firstSkipped !== FALSE && $lastSkipped !== FALSE) {
        $this->debug("Skipped $firstSkipped to $lastSkipped and starting work with " . $short_name);
        $firstSkipped = $lastSkipped = FALSE;
      }

      $projects_after = Settings::getSetting('projects_after');
      if ($projects_after && strcmp($short_name, $projects_after) < 0) {
        $this->debug("Skipping $project_name since it is before $projects_after\n");
        continue;
      }

      $debug_version = "$short_name - $version";
      $this->debug("$debug_version: evaluating");
      $patchInspector = new PatchInspector($file);
      if ($patchInspector->isOnlyUseStatements()) {
        $skipped++;
        $this->debug("$debug_version: Skipping only contains use statement changes");
        continue;
      }
      // We could limit supported projects by only supporting projects that
      // have certain usage level.
      // Can be determined by https://dispatcher.drupalci.org/job/phpstan/lastBuild/parameters/parameter/projects.tsv/projects.tsv
      // see https://git.drupalcode.org/project/deprecation_status/-/blob/script/stats.php
      $supported_projects[] = $short_name;
      $project = Project::createFromName($short_name, $version);
      $project->updateRecord();

      // Skip this project if there were errrors with this project.
      if ($this->skipPatch($short_name, $version)) {
        $skipped++;
        $this->debug("$debug_version: Skipping patch: $file");
        continue;
      }

      if ($project->skip($build_timestamp)) {
        $this->debug("$debug_version: Skipping");
        continue;
      }
      $issue_number = $project->getRectorIssue();
      $new_issue = FALSE;
      if (empty($issue_number)) {
        // Create the issue if none exists.
        try {
          $issue_number = $this->createRectorIssue($short_name, $version);
          $this->debug("$debug_version: Created issue $issue_number");
        }
        catch (\Exception $exception) {
          $this->logError("$debug_version: Could not create issue", $exception);
          continue;
        }
        if ($issue_number) {
          $new_issue = TRUE;
          $project->setRectorIssue($issue_number);
        }
      }
      if (empty($issue_number)) {
        $this->debug("$debug_version: No issue number, cannot continue", 'warning');
        continue;
      }
      $hash = md5_file($file);
      $diff_file = NULL;
      // If didn't just create this issue check to see if should be skipped.
      if (!$new_issue) {
        $this->debug("$debug_version: Checking existing issue: " . $issue_number);
        // Check to make sure the same exact patch was not already uploaded.
        if ($hash === $project->getRecordColumn('last_patch_hash')) {
          $this->debug("$debug_version: Hash equal skipping");
          continue;
        }
        // Load drupal.org issue with REST API.
        $issue = $this->getNode($issue_number);
        // Skip issues with unsupported status or where tag was removed.
        if ($this->skipIssue($issue)) {
          $this->debug("$debug_version: Skipping issue $issue_number based on status or lack of tag");
          continue;
        }
        if ($this->findCommentForBuildNum($issue_number, $build_num)) {
          $this->debug("$debug_version: Already posted patch for build $build_num, skipping");
          continue;
        }

        /**
         * Logic to ignore the fact a patch already exists. This is used to make sure that
         * contrib issues have a post rector file.
         */
        if (Config::getSetting('post_comment_when_post_rector_is_missing') === TRUE) {
          $info_is_updated = $this->updatedInfoYml(static::getBaseDir() . "/$file");
          if (!$info_is_updated) {
            // Upload the previous file first (done here, as Save would also
            // upload it and avoid one extra HTTP request this way).
            $post_rector_txt_path = $this->createRectorPostResultFile($short_name);
            $post_rector_available = !empty($post_rector_txt_path);
            $post_rector_was_posted = $this->hasPostRectorFile($issue);

            $ignore_existing_patch = Config::getSetting('post_comment_when_post_rector_is_missing') === TRUE && $post_rector_was_posted === FALSE && $post_rector_available === TRUE;
          }
        }

        // Create an interdiff file between the new and the already uploaded patch.
        $diff_file = $this->createDiffFile($file, $issue);
        $filesize = @filesize($diff_file);

        if ($filesize === FALSE) {
          $this->debug("$debug_version: interdiff size is FALSE, files are hidden, not there or you are running a dry-run.", "warning");
        }

        // @todo This might need changing, should perhaps also check if MR needs rebase? Then do a rebase. This path should not create a comment. For now only update when a new patch is available.



        if ($filesize === 0) {
          $this->debug("$debug_version: interdiff is 0 bytes, new patch is the same as already uploaded");
          // Save the hash of the patch to make next runs faster.
          $project->setRecord(['last_patch_hash' => $hash]);

          if ($ignore_existing_patch === FALSE) {
            continue;
          }
        }
        $this->debug("$debug_version: interdiff is " . $filesize . " bytes");
      }

      $comment_tokens['ignore_existing_patch'] = $ignore_existing_patch;

      if ($this->createCommentForPatch($project_name, $file, $issue_number, $comment_tokens, $diff_file)) {
        $this->createOrUpdateMrForPatch($project_name, $file, $issue_number, $comment_tokens);

        // The comment was created successfully. Store the file hash to ensure
        // we don't upload the exact same file 2x.
        $this->debug("$debug_version: created comment with patchfile");
        if (!empty($hash)) {
          $project->setRecord(['last_patch_hash' => $hash]);
        }
        else {
          $this->logError("$debug_version: Could not create hash for file $file");
        }
      }
    }

    if ($this->session) {
      $this->session->stop();
    }
    $output = "projects: " . count($supported_projects) . "\n"
      . "skipped patches: $skipped\n";
    file_put_contents('output.txt', $output);
    if ($this->dryRun) {
      return;
    }

    // @todo not sure yet what info we should save here.
    unset($build_info['artifacts'], $build_info['changeSet'], $build_info['actions']);
    $last_run = [
      'phpstan_run' => $build_info,
      'result' => 'SUCCESS',
    ];
    $last_run_cache->set($last_run);
    $cache->save($last_run_cache);
  }

  /**
   * Creates a project bot issue.
   *
   * @param $short_name
   *   The short name for the drupal.org project.
   * @param $version
   *   The version of the branch. This version number is after the `8.x-` prefix.
   *
   * @return null|int
   *   The issue number or null if no issue was created.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @todo Make the issue number for multiple major versions besides 8.x.
   *
   */
  private function createRectorIssue($short_name, $version): ?int {
    if ($this->dryRun) {
      return NULL;
    }
    //$session = $this->getSession();
    $this->visit("/node/add/project-issue/$short_name");

    $page = $this->getSession()->getPage();
    $issue_fields = Config::getSetting('new_issue_values');
    $issue_fields['edit-title'] = $issue_fields['edit-title'] . ' for ' . $short_name;

    foreach ($issue_fields as $key => $value) {
      if ($key !== 'edit-field-issue-component-und') {
        $page->fillField($key, $value);
      }
      else {
        try {
          $page->fillField('edit-field-issue-component-und', $value);
        }
        catch (\Exception $exception) {
          $page->fillField('edit-field-issue-component-und', "_none");
        }
      }
    }
    if ($page->hasField('edit-field-issue-version-und')) {
      try {
        $page->fillField('edit-field-issue-version-und', $version);
      }
      catch (\Exception $exception) {
        $this->logError("try $version", $exception);
        try {
          $page->fillField('edit-field-issue-version-und', "8.x-$version");
        }
        catch (\Exception $exception) {
          $this->logError("Tried 8.x-$version", $exception);
          $this->logError("Could not create issue $short_name-$version", $exception);
          return NULL;
        }
      }
    }
    $tokens = [
      'bot_tag' => Config::getSetting('bot_tag'),
    ];
    $issue_text = $this->getTemplateText('issue', $tokens);

    $page->fillField('edit-body-und-0-value', $issue_text);
    $page->pressButton('Save');
    if (strpos($page->getText(), 'has been created') === FALSE) {
      $this->debugOutput();
      $this->logError("$short_name $version: Issue could not be created");
      return NULL;
    }

    $url = $this->getSession()->getCurrentUrl();
    $url_parts = explode('/', $url);
    $issue_number = array_pop($url_parts);
    $this->debugOutput();
    return (int) $issue_number;
  }

  /**
   * Finds the existing comment for build number, if any.
   *
   * @param int $issue
   *   The issue number.
   * @param string $build_num
   *   The build number.
   *
   * @return \stdClass|NULL
   *   The comment or null if no comment was found.
   */
  private function findCommentForBuildNum($issue, $build_num) {
    // @todo dont things we should change this, just keep it as durpalcirun=
    $uid = Settings::getRequiredSetting('uid');
    // Load the last comment the bot user posted.
    $comment = Request::getSingle("/api-d7/comment.json?node=$issue&author=$uid&sort=created&direction=DESC&limit=1");
    if (!empty($comment->comment_body->value)) {
      $body = $comment->comment_body->value;
      // Identify comment based on div HTML attribute.
      if (strpos($body, "drupalcirun=\"$build_num\"") !== FALSE) {
        return $comment;
      }
    }
    return NULL;
  }

  private function createOrUpdateMrForPatch($project_name, $file, int $issue, array $comment_tokens, string $diff_file = NULL) {
    $this->debug("Handle merge request $issue");

    [$short_name, $version] = explode('.', $project_name, 2);

    // Check if the create merge request button is available.
    $this->visit("/node/$issue");
    $session = $this->getSession();

    $git_dir = static::getBaseDir() . '/phpstan_run';
    $git_project_dir = static::getBaseDir() . '/phpstan_run/project';
    if (file_exists($git_project_dir)) {
      exec('rm -rf ' . $git_project_dir);
    }

    $createIssueForkButton = $session->getPage()->findById('drupalorg-issue-fork-block-form');
    if ($createIssueForkButton !== NULL) {
      $this->debug("Creating issue fork for issue $issue");

      // Create a merge request.
      $session->getPage()->pressButton('Create issue fork');

      // Wait for the fork to be created.
      $session->getPage()->waitFor(10,
        function() use ($session) {
          return $session->getPage()->has('css', '.fork-link');
        }
      );

      $this->debug("Waiting for GitLab to create fork.");
      sleep(10);
    }

    $this->debug('Finding issue fork name');
    $issue_fork_name = $session->getPage()
      ->find('css', '.fork-link')
      ->getText();
    $git_url = 'git@git.drupal.org:issue/' . $issue_fork_name . '.git';

    chdir($git_dir);

    $this->debug('Cloning project');
    exec('git clone git@git.drupal.org:project/' . $short_name . '.git project');
    chdir($git_dir . '/project');

    $this->debug('Checking out branch for version ' . $version);

    $this->debug('Find target branch for MR');
    if (str_contains($version, '-dev')) {
      $this->debug('Dev version');
      $targetBranch = $this->findTargetBranchForDevVersion($version);
    }
    else {
      $this->debug('Non-dev version');
      $targetBranch = $this->findTargetBranchByVersion($version);
    }

    $this->debug('Target branch: ' . $targetBranch);

    // Version should never contain a -dev suffix.
    exec('git fetch --all');
    sleep(5);
    exec('git checkout -b project-update-bot-only origin/' . $targetBranch);

    $this->debug('Adding remote ' . $git_url);
    exec('git remote add update-bot ' . $git_url);
    exec('git fetch update-bot');

    $this->debug('Apply patch and commit to branch');
    exec('git apply ' . static::getBaseDir() . '/' . $file);
    exec('git add .');
    exec('git commit -m "Automated Project Update Bot fixes from run ' . $comment_tokens['run'] . '."');

    $this->debug('Pushing to remote');
    exec('git push update-bot project-update-bot-only --force -o merge_request.create -o merge_request.target=' . $targetBranch . ' -o merge_request.target_project="project/' . $short_name . '"  -o merge_request.title="Automated Project Update Bot fixes" -o merge_request.description="Relates to #' . $issue . '. This merge request was automatically created by the Project Update Bot. It contains the changes from run ' . $comment_tokens['run'] . '."');
  }

  /**
   * Creates a comment for a patch.
   *
   * @param $file
   * @param int $issue
   * @param array $comment_tokens
   *
   * @param string|null $diff_file
   *
   * @return bool|null
   *   TRUE if comment was created, FALSE it was not, otherwise NULL if not
   *   comment was needed.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  private function createCommentForPatch(string $project_name, $file, int $issue, array $comment_tokens, string $diff_file = NULL) {
    if ($this->dryRun) {
      return NULL;
    }
    $this->visit("/node/$issue");
    $file_path = static::getBaseDir() . "/$file";
    $page = $this->getSession()->getPage();

    [$short_name, $version] = explode('.', $project_name, 2);
    if ($page->hasField('edit-field-issue-version-und')) {
      $this->debug("Setting issue version $version");
      try {
        $page->fillField('edit-field-issue-version-und', $version);
      }
      catch (\Exception $exception) {
        $this->debug("try $version");
        try {
          $page->fillField('edit-field-issue-version-und', "8.x-$version");
        }
        catch (\Exception $exception) {
          $this->debug("Tried 8.x-$version couldn't set version");
        }
      }
    }

    $issue_fields = Config::getSetting('new_issue_values');
    $title_field = $page->findField('edit-title');

    $this->debug('Checking title: "' . $title_field->getValue() . '" against "' . $issue_fields['edit-title'] . '"');
    // If the title is still the original title update it.
    if ($title_field->getValue() == $issue_fields['edit-title']) {
      $page->fillField('edit-title', $issue_fields['edit-title'] . ' for ' . $short_name);
    }

    /*$issue_summary_field = $page->findField('edit-body-und-0-value');
    $summary = $issue_summary_field->getValue();
    $old_tag = 'RectorAutoPatches';
    // Issue were original created with the wrong tag. If the tag is still in
    // the summary replace it with the correct one.
    if (strpos($summary, $old_tag) !== FALSE) {
      $summary = str_replace($old_tag, Config::getSetting('bot_tag'), $summary);
      $issue_summary_field->setValue($summary);
    }*/

    // Set to needs review.
    $page->fillField('edit-field-issue-status-und', self::ISSUE_STATUS_NEEDS_REVIEW);

    $info_is_updated = $this->updatedInfoYml($file_path);

    // If the info file was not updated, there are error left that we know.
    $post_rector_txt_path = FALSE;
    if (!$info_is_updated) {
      // Upload the previous file first (done here, as Save would also
      // upload it and avoid one extra HTTP request this way).
      $page->pressButton('Upload');
      $post_rector_txt_path = $this->createRectorPostResultFile($short_name);
    }

    $comment_tokens['info_yml_updated'] = $info_is_updated;
    $comment_tokens['post_rector_results_posted'] = !empty($post_rector_txt_path);
    $body = $this->getTemplateText('comment', $comment_tokens);
    $page->fillField('edit-nodechanges-comment-comment-body-und-0-value', $body);

    // If there was an interdiff, add that first.
    if ($diff_file) {
      $this->debug("Uploading diff file: $diff_file");
      $page->find('css', 'input.form-file')->setValue($diff_file);
      $page->pressButton('Upload');
    }
    // Add the actual diff file as well.
    $this->debug("Uploading patch file: $file");
    $page->find('css', 'input.form-file')->setValue($file_path);
    $page->pressButton('Upload');

    if (!empty($post_rector_txt_path)) {
      // Upload the post-rector report file.
      $this->debug("Uploading post-rector file: $post_rector_txt_path");
      $page->find('css', 'input.form-file')->setValue($post_rector_txt_path);
      $page->pressButton('Upload');
    }

    // Save the comment (and upload the last file as well).
    $page->pressButton('Save');
    $this->debugOutput();

    // Verify the comment posting worked.
    $status_message = $page->find('css', '.messages.status');
    if (empty($status_message) || strpos($status_message->getText(), 'has been updated') === FALSE) {
      $this->logError("Comment could not be created for: $issue, $file");
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Gets template text with tokens replaced.
   *
   * The templates live in bot_templates.
   *
   * @param string $name
   *   The name of the template file without the `.html.twig` suffix.
   *   See https://git.drupalcode.org/project/infrastructure/-/tree/main/stats/project_analysis/bot_templates
   *   for which templates are available.
   * @param array $tokens
   *   The tokens as expected by \Twig\TemplateWrapper::render().
   *
   * @return string
   *   The rendered template.
   */
  private function getTemplateText(string $name, array $tokens = []) {
    /** @var \Twig\Loader\FilesystemLoader $loader */
    static $loader;
    /** @var \Twig\Environment $twig */
    static $twig;
    /** @var array $templates */
    static $templates;
    if (empty($loader) || empty($twig)) {
      $loader = new FilesystemLoader(__DIR__ . '/../../bot_templates');
      $twig = new Environment($loader);
    }
    if (empty($templates[$name])) {
      $templates[$name] = $twig->load("$name.html.twig");
    }
    return $templates[$name]->render($tokens);
  }

  /**
   * Determines if patch should be skipped due to error logs related to it.
   *
   * The current build job may produce 2 Update Status error files:
   *
   * [short_name].[version].upgrade_status_stderr1
   * [short_name].[version].upgrade_status_stderr2
   *
   * If this function finds errors in these files, the project should be skipped.
   *
   * @param string $short_name
   *   The project short name.
   * @param string $version
   *
   * @return bool
   *   Whether the patch should be skipped
   */
  private function skipPatch(string $short_name, string $version): bool {
    foreach ([1, 2] as $file_num) {
      $upgrade_status_error_file = static::RESULTS_DIR . "/$short_name/$short_name.$version.upgrade_status_stderr$file_num";
      if (file_exists($upgrade_status_error_file)) {
        $contents = file_get_contents($upgrade_status_error_file);
        if (strpos($contents, "[error]") !== FALSE || strpos($contents, "Fatal error:") !== FALSE) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Determine if the info file was updated in the patchfile.
   *
   * @todo Move to \RectorIssues\DrupalOrg\PatchInspector
   *
   * @param string $file_path
   *   The file path of the patch file.
   *
   * @return bool
   *   True if the info.yml for the project was updated in the patch.
   */
  private function updatedInfoYml(string $file_path): bool {
    $contents = file_get_contents($file_path);
    return preg_match('/diff --git.*info\.yml.*info\.yml/', $contents) === 1;
  }

  /**
   * Creates a diff file based on the last uploaded patch.
   *
   * The diff is between the patch file that is currently being uploaded and
   * the previous patch uploaded by bot if any.
   *
   * @param string $file_path
   *   The file path for
   * @param \stdClass $issue
   *   The drupal.org issue as returned by
   *   \RectorIssues\DrupalOrg\IssueBase::getNode().
   */
  private function createDiffFile(string $file_path, \stdClass $issue) {
    if ($this->dryRun) {
      return NULL;
    }
    if (empty($issue->field_issue_files)) {
      return NULL;
    }
    foreach (array_reverse($issue->field_issue_files) as $file_info) {
      if ($file_info->display === "0") {
        $this->debug("Skipping file {$file_info->file->id} since it is not displayed");
        continue;
      }

      $response = Request::get("/api-d7/file/{$file_info->file->id}.json")->send();
      if ($response->code !== 200 || $response->hasErrors() || !$response->hasBody() || !is_object($response->body)) {
        // If we can't get any particular file then we can't be sure what the
        // last file from the bot is.
        $this->logError("Could not get file {$file_info->file->id} from drupal.org");
        return NULL;
      }
      $file = $response->body;
      $owner_uid = (int) $file->owner->id;
      // The file was uploaded by the bot and is a patch.
      if ($owner_uid === (int) Settings::getRequiredSetting('uid') && strpos($file->name, '.patch') !== FALSE) {
        $contents = static::getDrupalOrgFile($file->url);
        if (empty($contents)) {
          $this->logError("Could not get file contents for {$file->url}");
          return NULL;
        }
        $previous_path = sys_get_temp_dir() . "/{$file->fid}.{$file->name}";
        file_put_contents($previous_path, $contents);
        $diff_path = sys_get_temp_dir() . "/interdiff-last-bot.{$file->fid}.txt";
        // Create an interdiff to compare the two patches.
        exec("interdiff $file_path $previous_path > $diff_path");
        return $diff_path;
      }
    }

    return NULL;
  }

  /**
   * Check if a rector post patch file has been posted.
   */
  private function hasPostRectorFile(\stdClass $issue): ?bool {
    $this->debug('Check for post-rector file.');
    if ($this->dryRun) {
      return NULL;
    }
    if (empty($issue->field_issue_files)) {
      return FALSE;
    }
    foreach (array_reverse($issue->field_issue_files) as $file_info) {
      if ($file_info->display === "0") {
        $this->debug("Skipping file {$file_info->file->id} since it is not displayed");
        continue;
      }

      $response = Request::get("/api-d7/file/{$file_info->file->id}.json")->send();
      if ($response->code !== 200 || $response->hasErrors() || !$response->hasBody() || !is_object($response->body)) {
        // If we can't get any particular file then we can't be sure what the
        // last file from the bot is.
        $this->logError("Could not get file {$file_info->file->id} from drupal.org");
        return FALSE;
      }
      $file = $response->body;
      $owner_uid = (int) $file->owner->id;
      // The file was uploaded by the bot and is a patch.
      if ($owner_uid === (int) Settings::getRequiredSetting('uid') && str_ends_with($file->name, '.post_rector.txt') !== FALSE) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Set limit (number of projects to process) and offset (index of project to start processing from).
   */
  public function setLimit(int $limit, int $offset) {
    $this->debug("Setting limit to $limit and offset to $offset");
    $this->limit = $limit;
    $this->offset = $offset;
  }

  /**
   * When offset and limit are set, decide if we should skip since we are below the offset or at/over the offset + limit.
   *
   * @param int $current
   *
   * @return bool
   */
  protected function shouldSkipNumber(int $current) {
    if (!isset($this->limit) || !isset($this->offset)) {
      return FALSE;
    }
    if ($current < $this->offset || $current >= $this->offset + $this->limit) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * When offset and limit are set, decide if we should stop iterating since we are over the offset + limit.
   *
   * @param int $current
   *
   * @return bool
   */
  protected function shouldBreak(int $current) {
    if (!isset($this->limit) || !isset($this->offset)) {
      return FALSE;
    }
    if ($current > $this->offset + $this->limit) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @param string $project_name
   * @param string $post_rector_txt_path
   *
   * @return string
   */
  public function createRectorPostResultFile(string $project_name): string {
    // Look for the post_rector report results.
    $files = glob(static::RESULTS_DIR . '/' . $project_name . '/*.post_rector.xml');
    if (empty($files)) {
      return '';
    }

    $txt_contents = '';
    $post_rector_xml = simplexml_load_file($files[0]);

    foreach ($post_rector_xml->file as $file_result) {
      // Start a new section in the text for each file.
      $file_name = $file_result['name'];
      $txt_contents .= $project_name . ' - ' . $file_name . ":\n";
      $table = [];
      foreach ($file_result->error as $error_result) {
        $error_message = str_replace("\n", ' ', $error_result['message']);
        $table[] = [
          'severity' => wordwrap($error_result['severity'], 8, "\n", TRUE),
          'line' => wordwrap($error_result['line'], 7, "\n", TRUE),
          'message' => $error_message,
        ];
      }
      // Format found errors in an ASCII table.
      $asciiRenderer = new ArrayToTextTable($table);
      $txt_contents .= $asciiRenderer->render() . "\n\n";
    }
    // Save the resulting content in a temporary text file for upload.
    $post_rector_txt_name = basename($files[0], '.xml') . '.txt';
    $post_rector_txt_path = sys_get_temp_dir() . "/" . $post_rector_txt_name;
    file_put_contents($post_rector_txt_path, $txt_contents);

    return $post_rector_txt_path;
  }

  public function findTargetBranchForDevVersion(string $version): string {
    $targetBranch = false;
    $branches = [];

    $versionNoDev = str_replace('-dev', '', $version);

    $this->debug('Find target branch based on ' . $version);
    exec('git branch -a | grep ' . $versionNoDev . ' | grep remotes/origin/ |  grep -v remotes/origin/HEAD | sed \'s/\s*remotes\/origin\///\'', $branches);

    // Find branches like: 2.x-dev -> 2.x, 2.0.x-dev -> 2.0.x
    $this->debug('Find branches like: 2.x-dev -> 2.x, 2.0.x-dev -> 2.0.x');
    foreach ($branches as $branch) {
      if ($versionNoDev === $branch) {
        $targetBranch = $branch;
        $this->debug('Found target branch ' . $branch . ' based on ' . $versionNoDev);
        return $targetBranch;
      }
    }

    $prefixes = [
      '8.x-',
      '9.x-',
      '10.x-',
      '11.x-',
    ];
    if ($targetBranch === FALSE) {
      foreach ($prefixes as $prefix) {
        exec('git branch -a | grep ' . $prefix . $versionNoDev . ' | grep remotes/origin/ | grep -v remotes/origin/HEAD | sed \'s/\s*remotes\/origin\///\'', $branches);

        // Find branches like: 2.x-dev -> 8.x-2.x
        $this->debug('Find branches like: 2.x-dev -> 8.x-2.x: Git branches - ' . print_r($branches, TRUE));
        foreach ($branches as $branch) {
          if ($branch == $prefix . $versionNoDev) {
            $targetBranch = $branch;
            $this->debug('Found target branch ' . $branch . ' based on (' . $prefix .')' . $versionNoDev);
            return $targetBranch;
          }
        }
      }
    }

    if ($targetBranch === FALSE) {
      $this->debug('Could not find target branch, using default');
      $targetBranch = $version;
    }
    return $targetBranch;
  }

  public function findTargetBranchByVersion(string $version): string {
    // find merge request target
    $branches = [];

    $this->debug('Find target branch based on ' . $version);
    exec('git branch -a --contains ' . $version . ' | grep remotes/origin/ | grep -v remotes/origin/HEAD | sed \'s/\s*remotes\/origin\///\'', $branches);
    foreach ($branches as $branch) {
      if (str_ends_with($branch, '.x')) {
        $branchNormalized = substr($branch, 0, -1);
        if (str_starts_with($version, $branchNormalized)) {
          $targetBranch = $branch;
          $this->debug('Found target branch based on ' . $version);
          return $targetBranch;
        }
      }
    }

    // Sometimes its a full version like 3.4.1 and the branch is 3.4.x
    if (!str_contains($version, '.x')) {
      $versionNormalized = substr($version, 0, -1) . 'x';

      $this->debug('Find target branch based on ' . $versionNormalized);
      exec('git branch -a --contains ' . $versionNormalized . ' | grep remotes/origin/ | grep -v remotes/origin/HEAD | sed \'s/\s*remotes\/origin\///\'', $branches);
      foreach ($branches as $branch) {
        if (str_ends_with($branch, '.x')) {
          $branchNormalized = substr($branch, 0, -1);
          if (str_starts_with($versionNormalized, $branchNormalized)) {
            $targetBranch = $branch;
            $this->debug('Found target branch based on ' . $versionNormalized);
            return $targetBranch;
          }
        }
      }

      // If we still didnt find a branch, it might be 3.x even. So we need to be even more aggresive.
      if (!str_contains($version, '.x')) {
        $versionNormalized = substr($version, 0, -3) . 'x';

        $this->debug('Find target branch based on ' . $versionNormalized);
        exec('git branch -a --contains ' . $versionNormalized . ' | grep remotes/origin/ | grep -v remotes/origin/HEAD | sed \'s/\s*remotes\/origin\///\'', $branches);
        foreach ($branches as $branch) {
          if (str_ends_with($branch, '.x')) {
            $branchNormalized = substr($branch, 0, -1);
            if (str_starts_with($version, $branchNormalized)) {
              $targetBranch = $branch;
              $this->debug('Found target branch based on ' . $versionNormalized);
              return $targetBranch;
            }
          }
        }
      }
    }

    $this->debug('Could not find target branch, using default');
    return $version;
  }

}
