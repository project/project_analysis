<?php

namespace RectorIssues\DrupalOrg;

/**
 * Inspects path files.
 */
class PatchInspector {

  /**
   * @var false|string
   */
  private $fileContents;

  /**
   * @var array
   */
  private $fileDiffs;

  public function __construct(string $file_path) {
    $this->fileContents = file_get_contents($file_path);
    if (!$this->fileContents) {
      return;
    }
    $this->fileDiffs = $this->getFileDiffs();
  }

  /**
   * Does the patch only add use statements for FQN uses.
   *
   * @return bool
   */
  public function isOnlyUseStatements(): bool {
    foreach ($this->fileDiffs as $path => $fileDiff) {
      if (empty($fileDiff['+']) || empty($fileDiff['-'])) {
        return FALSE;
      }
      if (count($fileDiff['+']) < 2 && count($fileDiff['-']) === 0) {
        return FALSE;
      }

      $additions = $fileDiff['+'];
      foreach ($additions as $addition) {
        $addition_trimmed = trim($addition);
        $addition_trimmed = substr($addition_trimmed, 1);
        if (str_starts_with($addition_trimmed, 'use ')) {
          $fqn = str_replace(';', '', str_replace('use ', '', $addition_trimmed));
          $line_indexes = array_keys($fileDiff['all_lines']);
          foreach ($line_indexes as $line_index) {
            if ($this->isSubtraction($fileDiff['all_lines'][$line_index])) {
              $line = $fileDiff['all_lines'][$line_index];
              $next_index = $line_index + 1;
              //$previous_index = $line_index - 1;
              if (!isset($fileDiff['all_lines'][$next_index])) {
                continue;
              }
              $next_line = $fileDiff['all_lines'][$next_index];
              if ($this->isAddition($next_line)) {
                $change_line = substr($line, 1);
                $change_next_line = substr($next_line, 1);
                if ($this->isFqnReplacement($fqn, $change_line, $change_next_line)) {
                  unset($fileDiff['+'][array_search($addition, $fileDiff['+'])]);
                  unset($fileDiff['-'][array_search($line, $fileDiff['-'])]);
                  unset($fileDiff['+'][array_search($next_line, $fileDiff['+'])]);
                }
              }
            }
          }

        }
      }
      if (!empty($fileDiff['+']) || !empty($fileDiff['-'])) {
        return false;
      }
    }
    return TRUE;
  }

  private function getFileDiffs(): array {
    $lines = explode("\n", $this->fileContents);
    $fileDiffs = [];
    foreach ($lines as $line) {
      if (empty($line)) {
        continue;
      }
      if (str_starts_with($line, 'diff --git')) {
        $parts = explode(' ', $line);
        $path = $parts[2];
        $path = substr($path, 2);
      }
      if (!isset($path)) {
        continue;
      }
      $fileDiffs[$path]['all_lines'][] = $line;
      if ($this->isAddition($line)) {
        $fileDiffs[$path]['+'][] = $line;
        $fileDiffs[$path]['all_changes'][] = $line;
      }
      elseif ($this->isSubtraction($line)) {
        $fileDiffs[$path]['-'][] = $line;
        $fileDiffs[$path]['all_changes'][] = $line;
      }
    }
    return $fileDiffs;
  }

  /**
   * @param $line
   *
   * @return bool
   */
  protected function isAddition($line): bool {
    return str_starts_with($line, '+') && !str_starts_with($line, '+++');
  }

  /**
   * @param $line
   *
   * @return bool
   */
  protected function isSubtraction($line): bool {
    return str_starts_with($line, '-') && !str_starts_with($line, '---');
  }

  /**
   * @param string $fqn
   *   Fully qualified name.
   * @param $line
   * @param $next_line
   *
   * @return bool
   */
  protected function isFqnReplacement(string $fqn, $line, $next_line): bool {
    $fqn_parts = explode("\\", $fqn);
    array_pop($fqn_parts);
    $withoutClass = implode("\\", $fqn_parts) . '\\';
    $line_without_class = str_replace($withoutClass, '', $line);
    $line_without_class_leading = str_replace("\\" . $withoutClass, '', $line);
    $is_replacement =  $next_line === $line_without_class ||
      $next_line === $line_without_class_leading;
    return $is_replacement;
  }

}
