<?php

namespace RectorIssues\DrupalOrg;

use Behat\Mink\Driver\BrowserKitDriver;
use Behat\Mink\Session;
use RectorIssues\Config;
use RectorIssues\Settings;
use RectorIssues\UtilsTrait;
use Symfony\Component\BrowserKit\HttpBrowser;

/**
 * Base class for dealing with drupal.org issues.
 */
abstract class IssueBase {

  use UtilsTrait;

  /**
   * @var bool
   */
  protected $dryRun = FALSE;

  // Issue statuses
  protected const ISSUE_STATUS_ACTIVE = "1";
  protected const ISSUE_STATUS_FIXED = "2";
  protected const ISSUE_STATUS_CLOSED_DUPLICATE = "3";
  protected const ISSUE_STATUS_POSTPONED = "4";
  protected const ISSUE_STATUS_CLOSED_WONT_FIX = "5";
  protected const ISSUE_STATUS_CLOSED_WORKS_AS_DESIGNED = "6";
  protected const ISSUE_STATUS_CLOSED_FIXED = "7";
  protected const ISSUE_STATUS_NEEDS_REVIEW = "8";
  protected const ISSUE_STATUS_NEEDS_WORK = "13";
  protected const ISSUE_STATUS_RTBC = "14";
  protected const ISSUE_STATUS_PATCH_TO_BE_PORTED = "15";
  protected const ISSUE_STATUS_POSTPONED_MAINTAINER_NEEDS_MORE_INFO = "16";
  protected const ISSUE_STATUS_CLOSED_OUTDATED = "17";
  protected const ISSUE_STATUS_CLOSED_CANNOT_REPRODUCE = "18";

  /**
   * CSS selector for pages with logged in user.
   * @var string
   */
  protected $loggedInSelector = 'body.logged-in';

  /**
   * The drupal.org username.
   *
   * @var string
   */
  protected $userName;

  /**
   * The drupal.org password.
   *
   * @var string
   */
  protected $userPass;

  /**
   * The mink session.
   *
   * @var Session|null
   */
  protected $session;

  /**
   * Whether the user is currently logged in.
   *
   * @var bool
   */
  public $loggedIn = FALSE;

  /**
   * Sets if the current run should be a dry run.
   *
   * @param bool $dryRun
   */
  public function setDryRun(bool $dryRun): void {
    $this->dryRun = $dryRun;
  }

  /**
   * Sets the username and password for the drupal.org user.
   *
   * @param $name
   * @param $password
   */
  public function setUserInfo($name, $password) {
    $this->userName = $name;
    $this->userPass = $password;
  }

  /**
   * Determines if an issue should be skipped.
   *
   * Will skip if in certain status or the bot tag has beenremoved.
   *
   * @param $issue
   *
   * @return bool
   */
  protected static function skipIssue($issue) {
    if ($issue) {
      $tid = Config::getSetting('bot_tag_tid');
      $not_skipped_statuses = [
        static::ISSUE_STATUS_NEEDS_REVIEW,
        static::ISSUE_STATUS_ACTIVE,
        static::ISSUE_STATUS_NEEDS_WORK,
        static::ISSUE_STATUS_RTBC,
      ];
      if (!in_array($issue->field_issue_status , $not_skipped_statuses)) {
        return TRUE;
      }
      // If bot tag was removed skip.
      return !static::issueHasTag($issue, $tid);
    }
    return TRUE;
  }

    public function login() {
        $session = $this->getSession();
        $page = $session->getPage();

        $session->visit(static::getSiteUrl() . '/user/login?destination=/project/project_analysis');

        $this->debugOutput();
        $page->pressButton('edit-openid-connect-client-keycloak-login');

        $this->debugOutput();
        //$this->client->getCookieJar()->set($cookie);
        $page->fillField('username', $this->userName);
        $page->fillField('password', $this->userPass);

        $page->pressButton('login');
        $result = $page->waitFor(10, function($page) {
            // @todo Is this still necessary now that we have know user agent?
            //   Doesn't really matter because only happens on login.
            return $page->find('css', $this->loggedInSelector);
        });

        if ($result === NULL) {
            $this->debugOutput();
            $this->logError('Not logged in after submitting form.', NULL, TRUE);
        }
        $this->loggedIn = TRUE;
        $this->debugOutput();
    }

    /**
   * Get a single node from the drupal.org REST API.
   *
   * @param int $nid
   *   The node id.
   *
   * @return \stdClass|null
   *   The node as returned by the drupal.org API.
   */
  protected function getNode(int $nid) {
    if ($node = Request::getSingle("/api-d7/node.json?nid=$nid")) {
      return $node;
    }
    $this->logError("Could not retrieve node $nid");
    return NULL;
  }

  /**
   * Gets the Mink session.
   *
   * If no session is set this function will start the session. It will also set
   * 'user_agent', and `basic_auth' from settings also.
   *
   * @return \Behat\Mink\Session
   */
  protected function getSession() {
    if (!$this->session) {
      $httpBrowser = new HttpBrowser();
      $driver = new BrowserKitDriver($httpBrowser);
      $driver->setRequestHeader('User-Agent', Settings::getRequiredSetting('user_agent'));
      if ($basic_auth = Settings::getSetting('basic_auth')) {
        $driver->setBasicAuth($basic_auth['name'], $basic_auth['pass']);
      }

      $this->session = new Session($driver);
      $this->session->start();
    }
    return $this->session;
  }

  /**
   * Visits a url and logins if needed.
   *
   * @param $url
   *   The URL to visit.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  protected function visit($url) {
    $session = $this->getSession();
    $page = $session->getPage();
    if (!$this->loggedIn) {
        $this->login();
    }

    $url = static::getSiteUrl() . $url;
    $session->visit($url);
    $this->debugOutput();
  }

  /**
   * Export page output html to the /debug directory.
   */
  protected function debugOutput() {
    if (!Settings::getSetting('debug_pages')) {
      return;
    }
    $session = $this->getSession();
    $url = $session->getCurrentUrl();
    $time = microtime(TRUE);
    $clean = str_replace( static::getSiteUrl(), '', $url);
    $clean = substr(preg_replace( "/[^a-zA-Z0-9_]/", '_', $clean), 0, 128);

    file_put_contents("debug/pages/$time-$clean.html", $session->getPage()->getOuterHtml());
  }

  /**
   * Determines if an issue has a tag.
   *
   * @param \stdClass $issue
   * @param int $tid
   *
   * @return bool
   */
  protected static function issueHasTag(\stdClass $issue, int $tid) {
    foreach ($issue->taxonomy_vocabulary_9 as $tag) {
      if (((int) $tag->id) === $tid) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Sets up the current run.
   */
  protected function setUp() {
    $this->debug("Using site url: " . static::getSiteUrl());
  }
}
