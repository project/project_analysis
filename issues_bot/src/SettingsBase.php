<?php

namespace RectorIssues;

use Symfony\Component\Yaml\Yaml;

/**
 * Common functions for getting settings from yml files.
 */
abstract class SettingsBase {

  use UtilsTrait;

  /**
   * The settings file.
   *
   * @var string
   */
  const FILE = '';


  /**
   * Get all settings.
   *
   * @return array
   */
  public static function getSettings() {
    static $settings = [];
    if (!isset($settings[static::FILE])) {
      $settings[static::FILE] = Yaml::parseFile(static::getBaseDir() . '/' . static::FILE);
    }
    return $settings[static::FILE];

  }

  /**
   * Gets an individual setting.
   *
   * @param string $key
   * @param null $default
   *   The default setting value.
   *
   * @return mixed|null
   *   The value of the setting or the default setting.
   */
  public static function getSetting(string $key, $default = NULL) {
    $settings = static::getSettings();
    if (getenv(strtoupper($key))) {
      return getenv(strtoupper($key));
    }
    return isset($settings[$key]) ? $settings[$key] : $default;
  }

    /**
     * Gets a required setting.
     *
     * @param string $key
     *
     * @return mixed|null
     *
     * @throws \Exception
     *   Thrown if setting is not available.
     */
  public static function getRequiredSetting(string $key) {
      $setting = static::getSetting($key);
      if ($setting === NULL) {
          throw new \UnexpectedValueException("Setting $key is required");
      }
      return $setting;
  }
}
