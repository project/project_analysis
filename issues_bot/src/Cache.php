<?php

namespace RectorIssues;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;

/**
 * A cache class for run bot run data.
 *
 * Currently, this is only used to store information about the last run.
 */
class Cache extends FilesystemAdapter {

}
