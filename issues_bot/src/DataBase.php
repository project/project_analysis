<?php

namespace RectorIssues;

use Envms\FluentPDO\Query;
use Envms\FluentPDO\Structure;

/**
 * Class to manage the SQLite Database.
 */
class DataBase extends Query {
  use UtilsTrait;

  private const FILE_LOCATION = 'db/rector.db';

  private static $instance;

  /**
   * Return the instance of the database.
   *
   * Creates only 1 instance to avoid DB locking.
   *
   * @return static
   */
  public static function getInstance() {
    if (!isset(self::$instance)) {
      self::$instance = new static();
    }
    return self::$instance;
  }

  /**
   * Gets the database file location.
   * @return string
   *   The database file location. The file location is based on the site URL so
   *   connecting to the dev.drupal.org will use a different database file.
   */
  public static function getDatabaseLocation() {
      $site_url = static::getSiteUrl();
      $base_name = str_replace('https://', '', $site_url);
      return "db/$base_name.db";
  }
  /**
   * {@inheritdoc}
   */
  public $exceptionOnError = TRUE;

  private function __construct(\PDO $pdo = NULL, ?Structure $structure = NULL) {
    if (!$pdo) {
      $pdo = new \PDO('sqlite:' . static::getDatabaseLocation());
    }
    parent::__construct($pdo, $structure);
//    $this->convertRead = TRUE;
  }

  /**
   * Casts DB columns to INTs.
   *
   * @param $fetchAll
   *   The result of a \Envms\FluentPDO\Queries\Select::fetchAll() call.
   *
   * @return array|bool|mixed
   */
  public static function castColumns($fetchAll) {
    $known_ints = [
      'nid',
      'rector_issue',
    ];
    if (is_bool($fetchAll)) {
      return $fetchAll;
    }
    elseif (is_array($fetchAll)) {
      foreach ($fetchAll as &$row) {
        foreach ($row as $key => $value) {
          if (in_array($key, $known_ints) && $value !== NULL) {
            $row[$key] = (int) $value;
          }
        }
      }
    }
    return $fetchAll;
  }

  /**
   * Creates all table necessary for this application.
   *
   * @throws \Exception
   */
  public function createTables() {
    if (file_exists(static::getDatabaseLocation())) {
      assert(filesize(static::getDatabaseLocation()) === 0);
    }
    $sql = <<<SQL
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "projects" (
	"nid"	INTEGER NOT NULL,
	"name"	TEXT NOT NULL,
	"rector_issue"	TEXT,
	"rector_status"	TEXT NOT NULL,
	"version"	TEXT NOT NULL,
	"last_patch_hash"	TEXT
);
CREATE UNIQUE INDEX IF NOT EXISTS "project_version" ON "projects" (
	"name"	ASC,
	"version"	ASC
);
COMMIT;
SQL;
    try {
      $pdo = new \PDO('sqlite:' . static::getDatabaseLocation());
      $pdo->setAttribute(\PDO::ATTR_ERRMODE,
        \PDO::ERRMODE_EXCEPTION);
      $pdo->exec($sql);
    }
    catch (\PDOException $exception) {
      $this->logError('Could not create table', $exception, TRUE);
    }

  }

  public function checkDatabaseHealth(): bool {
      if (!file_exists(static::getDatabaseLocation())) {
          return FALSE;
      }

      if(filesize(static::getDatabaseLocation()) === 0) {
          return FALSE;
      }

      try {
          $pdo = new \PDO('sqlite:' . static::getDatabaseLocation());
          $pdo->setAttribute(\PDO::ATTR_ERRMODE,
              \PDO::ERRMODE_EXCEPTION);
          $pdo->exec("SELECT * FROM projects");
      }
      catch (\PDOException $exception) {
          return FALSE;
      }

      return TRUE;
  }
}
