<?php

namespace RectorIssues;

/**
 * Project settings.
 *
 * The setting file is not committed to the repo.
 *
 * It has sensitive information.
 */
class Settings extends SettingsBase {

  public const FILE = 'settings.yml';

  /**
   * Returns the value of the `is_testing` setting.
   * @return mixed|null
   */
  public static function isTesting() {
    return static::getSetting('is_testing');
  }

}
