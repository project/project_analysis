<?php

/**
 * Script to clear the cache about the last run.
 */
use RectorIssues\Cache;

require_once __DIR__ . '/vendor/autoload.php';

$c = new Cache();
if ($c->clear()) {
  print "Cached cleared👍\n";

}
else {
  print "Nope😞";
}
