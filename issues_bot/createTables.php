<?php

/**
 * Script to create DB tables
 */
use RectorIssues\DataBase;

require_once __DIR__ . '/vendor/autoload.php';

$db = DataBase::getInstance();
$db->createTables();
