<?php


namespace RectorIssues\Tests\Unit;


use RectorIssues\DrupalOrg\Project;

class ProjectTest extends TestBase {

  /**
   * @dataProvider providerGetXMLRelease
   */
  public function testGetXMLRelease($file, $pattern, $expected_version) {
    $release = $this->getStaticMethodResult('getXMLRelease', $file, $pattern);
    if ($expected_version === NULL) {
      self::assertNull($release);
    }
    else {
      self::assertSame($expected_version,(string) $release->version);
    }

  }

  public function providerGetXMLRelease() {
    return [
      '8\.x-.*' => [
        'big_pipe_sessionless.xml',
        '8\.x-.*',
        '8.x-1.3',
      ],
      '7\.x-.*' => [
        'big_pipe_sessionless.xml',
        '7\.x-.*',
        NULL,
      ],
      '8\.x-.*-dev' => [
        'big_pipe_sessionless.xml',
        '8\.x-.*-dev',
        '8.x-1.x-dev',
      ],
    ];
  }

  /**
   * @param $file
   * @param $pattern
   * @param $expected
   *
   * @dataProvider providerGetReleaseTime
   */
  public function testGetReleaseTime($file, $pattern, $expected) {
    $time = $this->getStaticMethodResult('getReleaseTime', $file, $pattern);
    self::assertSame($expected, $time);
  }

  public function providerGetReleaseTime() {
    return [
      '8\.x-.*' => [
        'big_pipe_sessionless.xml',
        '8\.x-.*',
        1591256416,
      ],
      '7\.x-.*' => [
        'big_pipe_sessionless.xml',
        '7\.x-.*',
        NULL,
      ],
      '8\.x-.*-dev' => [
        'big_pipe_sessionless.xml',
        '8\.x-.*-dev',
        1591256249,
      ],
    ];
  }

  /**
   * @param $file
   * @param $pattern
   *
   * @return mixed
   */
  private function getStaticMethodResult($method, $file, $pattern) {
    $method = new \ReflectionMethod(Project::class, $method);
    $method->setAccessible(TRUE);
    $file_path = self::FIXTURE_DIR . $file;
    $xml = new \SimpleXMLElement(file_get_contents($file_path));
    return $method->invoke(NULL, $xml, $pattern);
}
}