<?php

namespace RectorIssues\Tests\Unit;

use RectorIssues\DrupalOrg\PatchInspector;

/**
 * @coversDefaultClass \RectorIssues\DrupalOrg\PatchInspector
 */
class PatchInspectorTest extends TestBase {

  /**
   * @param string $filePath
   * @param bool $expected
   *
   * @dataProvider providerIsOnlyUseStatements()
   */
  public function testIsOnlyUseStatements(string $filePath, bool $expected) {
    $i = new PatchInspector($filePath);
    $this->assertSame($expected, $i->isOnlyUseStatements());
  }

  public function providerIsOnlyUseStatements() {
    $test_cases = [];
    $only = glob(self::FIXTURE_DIR . '/only_use_statements/*');
    foreach ($only as $only_file) {
      $test_cases[basename($only_file). ' only use'] = [
        $only_file,
        TRUE,
      ];
    }
    $not_only = glob(self::FIXTURE_DIR . '/not_only_use_statements/*');
    foreach ($not_only as $not_only_file) {
      $test_cases[basename($not_only_file). ' NOT only use'] = [
        $not_only_file,
        FALSE,
      ];
    }
    return $test_cases;
  }

}
