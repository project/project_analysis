<?php


namespace RectorIssues\Tests\Unit;


use PHPUnit\Framework\TestCase;
use RectorIssues\DrupalOrg\IssueCreator;

/**
 * @coversDefaultClass \RectorIssues\DrupalOrg\IssueCreator
 */
class IssueCreatorTest extends TestBase {

  /**
   * @param $file
   *
   * @covers ::updatedInfoYml
   *
   * @dataProvider providerUpdatedInfoYml
   */
  public function testUpdatedInfoYml($file, $expected) {
    $creator = new IssueCreator();
    $method = new \ReflectionMethod(IssueCreator::class, 'updatedInfoYml');
    $method->setAccessible(TRUE);
    self::assertSame($expected, $method->invoke($creator, self::FIXTURE_DIR . $file));
  }

  public function providerUpdatedInfoYml() {
    return [
      [
        'azure_emotion_api.1.x-dev.rector.patch',
        TRUE,
      ],
      [
        'abookings.1.x-dev.rector.patch',
        FALSE,
      ],
      [
        'both.patch',
        TRUE,
      ],
    ];
  }

  public function testCreateRectorPostResultFileWithNoFiles(): void
  {
    $creator = new IssueCreator(); // Assume MyClass is the class where createRectorPostResultFile is defined

    $result = $creator->createRectorPostResultFile('test_project');

    $this->assertSame('', $result);
  }

  public function testCreateRectorPostResultFileWithValidFile(): void
  {
    // Prepare a dummy XML file to be processed
    $projectName = 'project_' . mt_rand(1000, 9999);
    $dummyFileContents = '<?xml version="1.0"?><checkstyle><file name="modules/contrib/' . $projectName . '/modules/acquia_cms_support/src/Controller/AcquiaCmsConfigSyncUnchanged.php"><error line="99" message="The \'acquia_cms_support/diff-modal\' library is not defined because the defining extension is not installed. Cannot decide if it is deprecated or not." severity="error"/></file><file name="modules/contrib/' . $projectName . '/modules/acquia_cms_support/src/Controller/AcquiaCmsConfigSyncOverridden.php"><error line="126" message="The \'acquia_cms_support/diff-modal\' library is not defined because the defining extension is not installed. Cannot decide if it is deprecated or not." severity="error"/></file><file name="modules/contrib/' . $projectName . '/modules/acquia_cms_support/src/Controller/AcquiaCmsConfigDiff.php"><error line="173" message="The \'acquia_cms_support/diff-modal\' library is not defined because the defining extension is not installed. Cannot decide if it is deprecated or not." severity="error"/></file><file name="modules/contrib/' . $projectName . '/' . $projectName .  '.info.yml"><error line="0" message="Value of core_version_requirement: ~10.2.0 is not compatible with the next major version of Drupal core. See https://drupal.org/node/3070687." severity="error"/></file><file name="modules/contrib/' . $projectName . '/modules/acquia_cms_support/acquia_cms_support.info.yml"><error line="0" message="Value of core_version_requirement: ^9.5 || ^10 is not compatible with the next major version of Drupal core. See https://drupal.org/node/3070687." severity="error"/></file><file name="modules/contrib/' . $projectName . '/modules/acquia_cms_development/acquia_cms_development.info.yml"><error line="0" message="Value of core_version_requirement: ^9.5 || ^10 is not compatible with the next major version of Drupal core. See https://drupal.org/node/3070687." severity="error"/></file><file name="modules/contrib/' . $projectName . '/composer.json"><error line="0" message="The drupal/core requirement is not compatible with the next major version of Drupal. Either remove it or update it to be compatible. See https://www.drupal.org/docs/develop/using-composer/add-a-composerjson-file#core-compatibility." severity="error"/></file></checkstyle>';

    chdir(sys_get_temp_dir());
    mkdir(IssueCreator::RESULTS_DIR . "/$projectName", 0777, TRUE);
    file_put_contents(IssueCreator::RESULTS_DIR . "/$projectName/dummy.post_rector.xml", $dummyFileContents);

    $myClass = new IssueCreator(); // Assume MyClass is the class where createRectorPostResultFile is defined

    $result = $myClass->createRectorPostResultFile($projectName);

    // Check if the returned path is correct and the file exists
    $this->assertStringContainsString(sys_get_temp_dir(), $result);
    $this->assertFileExists($result);

    // Optionally, inspect the contents of the generated text file for correctness
    $contents = file_get_contents($result);
    $this->assertStringContainsString("The 'acquia_cms_support/diff-modal' library is not defined", $contents);
    $this->assertStringContainsString("The drupal/core requirement is not compatible with the next", $contents);
  }

}
