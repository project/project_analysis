<?php
/**
 * Script to update issues.
 *
 * @see \RectorIssues\DrupalOrg\IssueUpdater
 */
use RectorIssues\DrupalOrg\IssueCreator;
use RectorIssues\DrupalOrg\IssueUpdater;
use RectorIssues\Settings;
use Seld\CliPrompt\CliPrompt;

require_once __DIR__ . '/vendor/autoload.php';
$user = Settings::getRequiredSetting('username');
echo "User password: ";

$pass = CliPrompt::hiddenPrompt();
echo "\nUser: $user\n";
if ($user && $pass) {
  $creator = new IssueUpdater();
  $creator->setUserInfo($user, $pass);
  $creator->updateIssues();
}

