<?php

/**
 * Script to check if database tables are healthy and recreate them if not.
 *
 * This script is used in CI to allow for database caching on branched. This way
 * we speed things up.
 */
use RectorIssues\DataBase;

require_once __DIR__ . '/vendor/autoload.php';

$db = DataBase::getInstance();
$healthy = $db->checkDatabaseHealth();

if ($healthy === FALSE) {
    echo ">>> Database is not healthy. Recreating tables.\n";
    exec('rm -f ./' . DataBase::getDatabaseLocation());
    $db->createTables();
} else {
    echo ">>> Database is healthy.\n";
}
