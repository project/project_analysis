<?php

/**
 * Main script to create issues and add comments to existing comments.
 *
 * @see \RectorIssues\DrupalOrg\IssueCreator
 */
use RectorIssues\DrupalOrg\IssueCreator;
use RectorIssues\Settings;
use Seld\CliPrompt\CliPrompt;

$return = shell_exec("which interdiff");
if (empty($return)) {
  die('interdiff is required. Please install using: `apt install patchutils`' . PHP_EOL);
}

require_once __DIR__ . '/vendor/autoload.php';

$options = getopt('', ['dry-run', 'offset:', 'limit:']);

$user = Settings::getRequiredSetting('username');
echo "User password: ";
if (getenv('ISSUE_BOT_PASSWORD')) {
  $pass = getenv('ISSUE_BOT_PASSWORD');
  echo "********\n";
} else {
  $pass = CliPrompt::hiddenPrompt();
}
echo "\nUser: $user\n";

if ($user && $pass) {
  $creator = new IssueCreator();
  $creator->setDryRun(isset($options['dry-run']));
  $creator->setUserInfo($user, $pass);

  if (isset($options['offset']) && isset($options['limit'])) {
    $creator->setLimit((int) $options['limit'], (int) $options['offset']);
  }

  $creator->createIssues();
}
