BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "projects" (
        "nid"   INTEGER NOT NULL,
        "name"  TEXT NOT NULL,
        "rector_issue"  TEXT,
        "rector_status" TEXT NOT NULL,
        "version"       TEXT NOT NULL,
        "last_patch_hash"       TEXT
);
CREATE UNIQUE INDEX IF NOT EXISTS "project_version" ON "projects" (
        "name"  ASC,
        "version"       ASC
);
COMMIT;
