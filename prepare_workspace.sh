#!/bin/bash
set -eu
# Set up this workspace to be ready to execute Upgrade Status and Drupal Rector.

export WORKSPACE_DIR="${WORKSPACE_DIR:-/build/drupalci/workspace}"
export CHECKOUT_DIR="${CHECKOUT_DIR:-/build/drupalci/drupal-checkout}"

git clone --quiet --shared ${CHECKOUT_DIR} ${WORKSPACE_DIR}/drupal-checkouts/drupal$1

cp -Rf ${WORKSPACE_DIR}/composer-home ${WORKSPACE_DIR}/drupal-checkouts/composer-home$1
export COMPOSER_HOME=${WORKSPACE_DIR}/drupal-checkouts/composer-home$1
composer --working-dir=${WORKSPACE_DIR}/drupal-checkouts/drupal$1 config prefer-stable false
