set -ex

export WORKSPACE_DIR="${WORKSPACE_DIR:-/build/drupalci/workspace}"
export CHECKOUT_DIR="${CHECKOUT_DIR:-/build/drupalci/drupal-checkout}"
export DRUPAL_CORE_VERSION="${DRUPAL_CORE_VERSION:-9.5.x}"

mkdir --parents ${WORKSPACE_DIR}/phpstan-results
chmod -R 777 ${WORKSPACE_DIR}
mkdir --parents ${WORKSPACE_DIR}/drupal-checkouts
mkdir --parents ${WORKSPACE_DIR}/composer-home

export COMPOSER_HOME=${WORKSPACE_DIR}/composer-home

sudo apt-get -qqq update
sudo apt-get -qqq install -y unzip time apt-utils parallel

git clone -b ${DRUPAL_CORE_VERSION} https://git.drupalcode.org/project/drupal.git ${CHECKOUT_DIR}

git config --global user.email "git@drupal.org"
git config --global user.name "Drupalci Testbot"
git config --global init.defaultBranch master

composer --working-dir=${CHECKOUT_DIR} config platform --unset
composer --working-dir=${CHECKOUT_DIR} config minimum-stability stable
composer --working-dir=${CHECKOUT_DIR} require drupal/core-recommended:${DRUPAL_CORE_VERSION}-dev -W
composer --working-dir=${CHECKOUT_DIR} require palantirnet/drupal-rector --prefer-stable --dev -W
composer --working-dir=${CHECKOUT_DIR} require drupal/upgrade_status --prefer-stable -W
composer --working-dir=${CHECKOUT_DIR} require drush/drush:^12.0 --prefer-stable --no-progress -W
composer --working-dir=${CHECKOUT_DIR} config minimum-stability dev

composer config --working-dir=${CHECKOUT_DIR} repositories.local "{\"type\": \"path\", \"url\": \"${CI_PROJECT_DIR}/project_analysis_utils\", \"options\": { \"symlink\": false}}"

# We do not want phpunit rules to run in PHPStan since those contain best-practices, so we ignore the phpstan-phpunit extension
composer config --working-dir=${CHECKOUT_DIR} --merge --json extra.phpstan/extension-installer.ignore '["phpstan/phpstan-phpunit"]'

composer --working-dir=${CHECKOUT_DIR} require drupalorg_infrastructure/project_analysis_utils --no-progress
sudo chmod 777 ${CHECKOUT_DIR}/sites/default
${CHECKOUT_DIR}/vendor/bin/drush -r ${CHECKOUT_DIR} si --db-url=sqlite://sites/default/files/.ht.sqlite -y
${CHECKOUT_DIR}/vendor/bin/drush -r ${CHECKOUT_DIR} en upgrade_status -y
git -C ${CHECKOUT_DIR} add sites/default/files/.ht.sqlite
#find ${CHECKOUT_DIR}/vendor -name .git -exec rm -rf {};

git -C ${CHECKOUT_DIR} add .
git -C ${CHECKOUT_DIR} commit -q -m "Add Upgrade Status, Drupal Rector, and configure them"
cp ${CHECKOUT_DIR}/composer.lock ${WORKSPACE_DIR}/phpstan-results/drupal-composer.lock.json
